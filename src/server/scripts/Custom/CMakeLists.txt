# Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

set(scripts_STAT_SRCS
  ${scripts_STAT_SRCS}
Custom/sistema_pve.cpp
Custom/well_of_eternity_portal.cpp
Custom/h_o_t_portal.cpp
Custom/dk_up.cpp
Custom/dk_down.cpp
Custom/dk_up_map1.cpp
Custom/dk_down_map1.cpp
Custom/firelands_volcanus.cpp
Custom/teleport_hyjal.cpp
Custom/teleport_hyjal_up.cpp
Custom/guildhouse.cpp
Custom/teleport_firelands_baston.cpp
Custom/promocioneshorda.cpp
Custom/promocionesali.cpp
Custom/npc_prof.cpp
Custom/npc_transmogrify.cpp
Custom/sistemavip.cpp
Custom/promocioneshordaVIP.cpp
Custom/promocionesaliVIP.cpp
Custom/vip_vendor.cpp
Custom/zonavip.cpp
Custom/fakeplayers.cpp

)

message("  -> Prepared: Custom")
