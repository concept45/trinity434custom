/* Creado para BoT WOW
NPC PVP
Por luis y bot
 */

#include "ScriptPCH.h"
#include "ScriptMgr.h"
#include "Common.h"
#include "Chat.h"
#include "Player.h"
#include "World.h"
#include "Config.h"
#include "WorldSession.h"
#include "Language.h"
#include "Log.h"
#include "SpellAuras.h"

class npc_promoaliVIP : public CreatureScript 

{
public:
    npc_promoaliVIP() : CreatureScript("npc_promoaliVIP") { } 
 
    bool OnGossipHello(Player* player, Creature* creature) OVERRIDE 
    {
        player->ADD_GOSSIP_ITEM(7, "Elige que personaje quieres equiparte, y despues habla con el de PVE.", GOSSIP_SENDER_MAIN, 99);
		switch (player->getClass())
		{
				case CLASS_DRUID: player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Equilibrio", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+30); player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Feral", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+44); player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Restauracion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+45); break;
				case CLASS_SHAMAN: player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Elemental", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+31); player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Mejora", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+46); player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Restauracion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+47); break;
				case CLASS_PALADIN: player->ADD_GOSSIP_ITEM(10, "Promocion de Paladin Represion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+32); player->ADD_GOSSIP_ITEM(10, "Promocion de Paladin Sagrado", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+40); break;
				case CLASS_WARRIOR: player->ADD_GOSSIP_ITEM(10, "Promocion de Guerrero DPS", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+33); break;
				case CLASS_PRIEST: player->ADD_GOSSIP_ITEM(10, "Promocion de Sacerdote Sombras", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+34); player->ADD_GOSSIP_ITEM(10, "Promocion de Sacerdote Sagrado o Disciplina", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+42); break;
				case CLASS_DEATH_KNIGHT: player->ADD_GOSSIP_ITEM(10, "Promocion de DK Escarcha", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+35); player->ADD_GOSSIP_ITEM(10, "Promocion de DK Profano", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+49); break;
				case CLASS_ROGUE: player->ADD_GOSSIP_ITEM(10, "Promocion de Picaro", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+36); break;
				case CLASS_HUNTER: player->ADD_GOSSIP_ITEM(10, "Promocion de Cazador", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+37); break;
				case CLASS_MAGE: player->ADD_GOSSIP_ITEM(10, "Promocion de Mago", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+38); break;
				case CLASS_WARLOCK: player->ADD_GOSSIP_ITEM(10, "Promocion de Brujo", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+39); break;
		}
                player->SEND_GOSSIP_MENU(1, creature->GetGUID()); 
                return true;
    }
 
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 actions) OVERRIDE
    {
		if(!player->GetSession()->IsPremium())
		{
			player->GetSession()->SendAreaTriggerMessage("No eres un miembro VIP.");
			return false;
		}
	    
		if (player->getLevel() == 85) 
		{
            player->GetSession()->SendAreaTriggerMessage("No puedes recibir el equipo mas de una vez por personaje.");
			player->CLOSE_GOSSIP_MENU();
				return true;
        }
		
        if (player->getLevel() == 1) 
        {
            uint32 accountID = player->GetSession()->GetAccountId();
            QueryResult result = CharacterDatabase.PQuery("SELECT COUNT(`guid`) FROM `characters` WHERE `account`=%u", accountID);
            Field *fields = result->Fetch();
            uint32 personajes = fields[0].GetUInt32();

            if (personajes < 6)		
                    {
					    if (actions == 99)
						{
						    player->CLOSE_GOSSIP_MENU();
						    return false;
						}
						player->GetSession()->SendAreaTriggerMessage("Has recibido tu equipo pvp, ahora habla con el maestro de pve para ser 85.");
						player->GiveLevel(50);
						switch(actions)
                              {								  
								  case GOSSIP_ACTION_INFO_DEF+30: // Druida Equilibrio PVP
								  player->AddItem(72346, 1);
								  player->AddItem(72326, 1);
								  player->AddItem(72357, 1);
								  player->AddItem(72356, 1);
								  player->AddItem(72323, 1);
								  player->AddItem(72352, 1);
								  player->AddItem(72353, 1);
								  player->AddItem(72350, 1);
								  player->AddItem(72347, 1);
								  player->AddItem(72351, 1);
								  player->AddItem(70181, 1);
								  player->AddItem(78630, 1);
								  player->AddItem(72448, 1);
							      player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+44: // Druida Feral PVP
								  player->AddItem(72338, 1);
								  player->AddItem(72307, 1);
								  player->AddItem(72341, 1);
								  player->AddItem(72340, 1);
								  player->AddItem(72305, 1);
								  player->AddItem(72421, 1);
								  player->AddItem(72337, 1);
								  player->AddItem(72417, 1);
								  player->AddItem(72339, 1);
								  player->AddItem(72419, 1);
								  player->AddItem(70228, 1);
								  player->AddItem(72310, 1);
								  player->AddItem(72304, 1);
								  player->AddItem(72309, 1);
								  player->AddItem(72312, 1);
								  player->AddItem(72311, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+45: // Druida Restauracion PVP
								  player->AddItem(72354, 1);
								  player->AddItem(72326, 1);
								  player->AddItem(72349, 1);
								  player->AddItem(72348, 1);
								  player->AddItem(72323, 1);
								  player->AddItem(72344, 1);
								  player->AddItem(72345, 1);
								  player->AddItem(72342, 1);
								  player->AddItem(72347, 1);
								  player->AddItem(72343, 1);
								  player->AddItem(70180, 1);
								  player->AddItem(77083, 1);
								  player->AddItem(72448, 1);
								  player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+31: // Chaman Elemental PVP
								  player->AddItem(72434, 1);
								  player->AddItem(72326, 1);
								  player->AddItem(72447, 1);
								  player->AddItem(72443, 1);
								  player->AddItem(72323, 1);
								  player->AddItem(72430, 1);
								  player->AddItem(72444, 1);
								  player->AddItem(72442, 1);
								  player->AddItem(72435, 1);
								  player->AddItem(72428, 1);
								  player->AddItem(70226, 1);
								  player->AddItem(78630, 1);
								  player->AddItem(72448, 1);
								  player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+46: // Chaman Mejora PVP
								  player->AddItem(72439, 1);
								  player->AddItem(72307, 1);
								  player->AddItem(72441, 1);
								  player->AddItem(72437, 1);
								  player->AddItem(72305, 1);
								  player->AddItem(72367, 1);
								  player->AddItem(72438, 1);
								  player->AddItem(72363, 1);
								  player->AddItem(72440, 1);
								  player->AddItem(72365, 1);
								  player->AddItem(70217, 1);
								  player->AddItem(70218, 1);
								  player->AddItem(72310, 1);
								  player->AddItem(72304, 1);
								  player->AddItem(72309, 2);
								  player->AddItem(72312, 1);
								  player->AddItem(72311, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+47: // Chaman Restauracion PVP
								  player->AddItem(72445, 1);
								  player->AddItem(72326, 1);
								  player->AddItem(72436, 1);
								  player->AddItem(72432, 1);
								  player->AddItem(72323, 1);
								  player->AddItem(72431, 1);
								  player->AddItem(72433, 1);
								  player->AddItem(72427, 1);
								  player->AddItem(72435, 1);
								  player->AddItem(72429, 1);
								  player->AddItem(70227, 1);
								  player->AddItem(77083, 1);
								  player->AddItem(72448, 1);
								  player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+32: // Paladin Represion PVP
								  player->AddItem(72380, 1);
								  player->AddItem(72453, 1);
								  player->AddItem(72382, 1);
								  player->AddItem(72378, 1);
								  player->AddItem(72451, 1);
								  player->AddItem(72398, 1);
								  player->AddItem(72379, 1);
								  player->AddItem(72394, 1);
								  player->AddItem(72381, 1);
								  player->AddItem(72396, 1);
								  player->AddItem(70231, 1);
								  player->AddItem(64674, 1);
								  player->AddItem(72450, 1);
								  player->AddItem(72455, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+40: // Paladin Healer PVP
								  player->AddItem(72391, 1);
								  player->AddItem(72327, 1);
								  player->AddItem(72393, 1);
								  player->AddItem(72389, 1);
								  player->AddItem(72324, 1);
								  player->AddItem(72388, 1);
								  player->AddItem(72390, 1);
								  player->AddItem(72384, 1);
								  player->AddItem(72392, 1);
								  player->AddItem(72386, 1);
								  player->AddItem(72448, 1);
								  player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72331, 1);
								  player->AddItem(70223, 1);
								  player->AddItem(70243, 1);
								  player->AddItem(77083, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+34: // Sacerdote Sombras PVP
								  player->AddItem(72406, 1);
								  player->AddItem(72326, 1);
								  player->AddItem(72409, 1);
								  player->AddItem(72408, 1);
								  player->AddItem(72323, 1);
								  player->AddItem(72319, 1);
								  player->AddItem(72405, 1);
								  player->AddItem(72407, 1);
								  player->AddItem(72313, 1);
								  player->AddItem(72317, 1);
								  player->AddItem(70181, 1);
								  player->AddItem(70196, 1);
								  player->AddItem(72448, 1);
								  player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+42: // Sacerdote Sagrado PVP
								  player->AddItem(72406, 1);
								  player->AddItem(72326, 1);
								  player->AddItem(72404, 1);
								  player->AddItem(72403, 1);
								  player->AddItem(72323, 1);
								  player->AddItem(72321, 1);
								  player->AddItem(72400, 1);
								  player->AddItem(72407, 1);
								  player->AddItem(72315, 1);
								  player->AddItem(72318, 1);
								  player->AddItem(70180, 1);
								  player->AddItem(70195, 1);
								  player->AddItem(72448, 1);
								  player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+33: // Guerrero Dps PVP
								  player->AddItem(72466, 1);
								  player->AddItem(72453, 1);
								  player->AddItem(72468, 1);
								  player->AddItem(72464, 1);
								  player->AddItem(72451, 1);
								  player->AddItem(72398, 1);
								  player->AddItem(72465, 1);
								  player->AddItem(72394, 1);
								  player->AddItem(72467, 1);
								  player->AddItem(72396, 1);
								  player->AddItem(70231, 1);
								  player->AddItem(70213, 1);
								  player->AddItem(70232, 1);
								  player->AddItem(72450, 1);
								  player->AddItem(72455, 1);
								  player->AddItem(72457, 2);
								  player->AddItem(72458, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+35: // DK Escarcha PVP
								  player->AddItem(72334, 1);
								  player->AddItem(72453, 1);
								  player->AddItem(72336, 1);
								  player->AddItem(72332, 1);
								  player->AddItem(72451, 1);
								  player->AddItem(72398, 1);
								  player->AddItem(72333, 1);
								  player->AddItem(72394, 1);
								  player->AddItem(72335, 1);
								  player->AddItem(72396, 1);
								  player->AddItem(70231, 1);
								  player->AddItem(71147, 1);
								  player->AddItem(72450, 1);
								  player->AddItem(72455, 1);
								  player->AddItem(72457, 1);
								  player->AddItem(72458, 2);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+49: // DK Profano PVP
								  player->AddItem(72334, 1);
								  player->AddItem(72453, 1);
								  player->AddItem(72336, 1);
								  player->AddItem(72332, 1);
								  player->AddItem(72451, 1);
								  player->AddItem(72398, 1);
								  player->AddItem(72333, 1);
								  player->AddItem(72394, 1);
								  player->AddItem(72335, 1);
								  player->AddItem(72396, 1);
								  player->AddItem(70231, 1);
								  player->AddItem(71147, 1);
								  player->AddItem(72450, 1);
								  player->AddItem(72455, 1);
								  player->AddItem(72457, 1);
								  player->AddItem(72458, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+36: // Picaro PVP
								  player->AddItem(72424, 1);
								  player->AddItem(72307, 1);
								  player->AddItem(72426, 1);
								  player->AddItem(72422, 1);
								  player->AddItem(72305, 1);
								  player->AddItem(72421, 1);
								  player->AddItem(72423, 1);
								  player->AddItem(72425, 1);
								  player->AddItem(72417, 1);
								  player->AddItem(72419, 1);
								  player->AddItem(70214, 1);
								  player->AddItem(70215, 1);
								  player->AddItem(70233, 1);
								  player->AddItem(72304, 1);
								  player->AddItem(72309, 1);
								  player->AddItem(72312, 1);
								  player->AddItem(72311, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+37: // Cazador PVP
								  player->AddItem(72370, 1);
								  player->AddItem(72307, 1);
								  player->AddItem(72372, 1);
								  player->AddItem(72368, 1);
								  player->AddItem(72305, 1);
								  player->AddItem(72367, 1);
								  player->AddItem(72369, 1);
								  player->AddItem(72363, 1);
								  player->AddItem(72371, 1);
								  player->AddItem(72365, 1);
								  player->AddItem(70225, 1);
								  player->AddItem(70236, 1);
								  player->AddItem(72304, 1);
								  player->AddItem(72309, 1);
								  player->AddItem(72312, 1);
								  player->AddItem(72311, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+38: // Mago PVP
								  player->AddItem(72374, 1);
								  player->AddItem(72326, 1);
								  player->AddItem(72377, 1);
								  player->AddItem(72376, 1);
								  player->AddItem(72323, 1);
								  player->AddItem(72319, 1);
								  player->AddItem(72373, 1);
								  player->AddItem(72375, 1);
								  player->AddItem(72313, 1);
								  player->AddItem(72317, 1);
								  player->AddItem(70181, 1);
								  player->AddItem(70196, 1);
								  player->AddItem(72448, 1);
								  player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+39: // Brujo PVP
								  player->AddItem(72460, 1);
								  player->AddItem(72326, 1);
								  player->AddItem(72463, 1);
								  player->AddItem(72462, 1);
								  player->AddItem(72323, 1);
								  player->AddItem(72319, 1);
								  player->AddItem(72459, 1);
								  player->AddItem(72461, 1);
								  player->AddItem(72313, 1);
								  player->AddItem(72317, 1);
								  player->AddItem(70181, 1);
								  player->AddItem(70196, 1);
								  player->AddItem(72448, 1);
								  player->AddItem(72449, 1);
								  player->AddItem(72329, 1);
								  player->AddItem(72330, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
	                          }
                    }			
			
					if (personajes > 5)
                    {
                        player->GetSession()->SendAreaTriggerMessage("No puedes equiparte más de 5 personajes, deberás borrarte alguno para recibir más objetos..");
						player->CLOSE_GOSSIP_MENU();
							return true;
                    }
					
		    player->CLOSE_GOSSIP_MENU();
        }
        return true;
    }
};
 
void AddSC_npc_promoaliVIP() 
{
    new npc_promoaliVIP(); 
}