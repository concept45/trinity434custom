/*
 * Copyright (C) 2013-2014 Sovak <sovak007@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "end_time.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "MoveSplineInit.h"

enum Spells
{
    SPELL_MOONLANCE                 = 102151,
    SPELL_MOONLANCE_AURA            = 102150,
    SPELL_MOONLANCE_DIVIDE          = 102152,
    SPELL_EYES_OF_THE_GODDES        = 102606,
    SPELL_PIERCING_GAZE_OF_ELUNE    = 102182,
    SPELL_MOONBOLT                  = 102193,
    SPELL_STARDUST                  = 102173,
    SPELL_TEARS_OF_ELUNE            = 102241,
    SPELL_LUNAR_GUIDANCE            = 102472

};

enum Events
{
    EVENT_EYES_OF_THE_GODDES    = 1
};

enum NPCs
{
    NPC_ECHO_OF_TYRANDE     = 54544,
    NPC_MOONLANCE_DIVIDE    = 54580
};

enum CreatureText
{
    SAY_INTRO1  = 0,
    SAY_INTRO2  = 1,
    SAY_INTRO3  = 2,
    SAY_INTRO4  = 3,
    SAY_INTRO5  = 4,
    SAY_AGGRO   = 5,
    SAY_SPELL1  = 6,
    SAY_SPELL2  = 7,
    SAY_SLAY    = 8,
    SAY_EVENT1  = 9,
    SAY_EVENT2  = 10,
    SAY_EVENT3  = 11,
    SAY_DEATH   = 12
};

#define EYES_DISTANCE 30.f

class boss_echo_of_tyrande : public CreatureScript
{
public:
    boss_echo_of_tyrande() : CreatureScript("boss_echo_of_tyrande") 
    {
    }

    BossAI* GetAI(Creature* creature) const
    {
        return new boss_echo_of_tyrandeAI(creature);
    }

    struct boss_echo_of_tyrandeAI : public BossAI
    {
        boss_echo_of_tyrandeAI(Creature* creature) : BossAI(creature, DATA_ECHO_OF_TYRANDE)
        {
            instance = me->GetInstanceScript();
            saidIntro = false;
        }

        InstanceScript* instance;
        bool summon;
        uint32 timer;
        bool saidIntro;

        void Reset()
        {
            summon = false;
            timer = 500;
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PACIFIED);
            me->RemoveAllAuras();
            me->SetHealth(me->GetMaxHealth());
            me->CombatStop(true);
        }

        void MoveInLineOfSight(Unit* who)
        {
            if (who->ToPlayer())
            {
                if (me->IsWithinDist(who, 50.f, true) && !saidIntro)
                {
                    saidIntro = true;
                    Talk(urand(SAY_INTRO1, SAY_INTRO5));
                }
            }
        }

        void EnterEvadeMode()
        {
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_TYRANDE, FAIL);

            me->GetMotionMaster()->MoveTargetedHome();
            Reset();
        }

        void EnterCombat(Unit* who) 
        {
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_TYRANDE, IN_PROGRESS);

            Talk(SAY_AGGRO);
            events.ScheduleEvent(EVENT_EYES_OF_THE_GODDES, 28000);
        }

        void JustDied(Unit* killer)
        {
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_TYRANDE, DONE);
            Talk(SAY_DEATH);
        }

        void KilledUnit(Unit* victim)
        {
            Talk(SAY_SLAY);
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_EYES_OF_THE_GODDES:
                    {
                        summon = true;
                        events.ScheduleEvent(EVENT_EYES_OF_THE_GODDES, 28000);
                        break;
                    }
                }
            }

            if (!me->GetCurrentSpell(CURRENT_GENERIC_SPELL))
            {
                if (timer <= diff)
                {
                    timer = 1500;
                    if (HealthBelowPct(30) && !me->HasAura(SPELL_TEARS_OF_ELUNE))
                    {
                        Talk(SAY_EVENT3);
                        DoCast(SPELL_TEARS_OF_ELUNE);
                        return;
                    }

                    if (HealthBelowPct(80) && !me->HasAura(SPELL_LUNAR_GUIDANCE))
                    {
                        Talk(SAY_EVENT1);
                        DoCast(SPELL_LUNAR_GUIDANCE);
                        return;
                    }

                    if (HealthBelowPct(55))
                    {
                        if (Aura* aura = me->GetAura(SPELL_LUNAR_GUIDANCE))
                        {
                            if (aura->GetStackAmount() < 2)
                            {
                                Talk(SAY_EVENT2);
                                DoCast(SPELL_LUNAR_GUIDANCE);
                                return;
                            }
                        }
                    }

                    if (summon)
                    {
                        Talk(SAY_SPELL1);
                        DoCast(SPELL_EYES_OF_THE_GODDES);
                        summon = false;
                        return;
                    }

                    switch (urand(0, 2))
                    {
                        case 0:
                            DoCast(SPELL_MOONBOLT);
                            break;
                        case 1:
                            DoCast(SPELL_STARDUST);
                            break;
                        case 2:
                            // Too much spam by this talk ^^
                            // Talk(SAY_SPELL2);
                            DoCast(SPELL_MOONLANCE);
                            break;
                        default:
                            break;
                    }
                }
                else
                    timer -= diff;
            }
        }
    };
};

class npc_moonlace_single : public CreatureScript
{
public:
    npc_moonlace_single() : CreatureScript("npc_moonlace_single") 
    {
    }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_moonlace_singleAI(creature);
    }

    struct npc_moonlace_singleAI : public CreatureAI
    {
        npc_moonlace_singleAI(Creature* creature) : CreatureAI(creature)
        {
            instance = me->GetInstanceScript();
        }
        
        InstanceScript* instance;
        uint32 timer;

        void Reset()
        {
            timer = 2000;
            me->AddAura(SPELL_MOONLANCE_AURA, me);
        }

        void UpdateAI(uint32 diff)
        {
            Unit* tyrande = me->FindNearestCreature(NPC_ECHO_OF_TYRANDE, VISIBLE_RANGE, true);

            if (!tyrande)
            {
                me->DespawnOrUnsummon();
                return;
            }

            me->SetOrientation(tyrande->GetAngle(me->GetPositionX(), me->GetPositionY()));
            float x = me->GetPositionX() + (VISIBLE_RANGE * cos(me->GetOrientation()));
            float y = me->GetPositionY() + (VISIBLE_RANGE * sin(me->GetOrientation()));
            float z = me->GetPositionZ();
            me->UpdateAllowedPositionZ(x, y, z);

            me->GetMotionMaster()->MovePoint(0, x, y, z);

            if (timer <= diff)
            {
                for (int32 i = 0; i < 3; i++)
                    tyrande->SummonCreature(NPC_MOONLANCE_DIVIDE, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), tyrande->GetAngle(me->GetPositionX(), me->GetPositionY()) - 0.3f + (0.3f * float (i)));
                me->DespawnOrUnsummon();
            }
            else
                timer -= diff;
        }
    };
};

class npc_moonlace_divided : public CreatureScript
{
public:
    npc_moonlace_divided() : CreatureScript("npc_moonlace_divided") 
    {
    }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_moonlace_dividedAI(creature);
    }

    struct npc_moonlace_dividedAI : public CreatureAI
    {
        npc_moonlace_dividedAI(Creature* creature) : CreatureAI(creature)
        {
            instance = me->GetInstanceScript();
        }
        
        InstanceScript* instance;
        uint32 timer;
        uint32 auraTimer;

        void Reset()
        {
            timer = 7000;
            auraTimer = 100;
        }

        void UpdateAI(uint32 diff)
        {
            Unit* tyrande = me->FindNearestCreature(NPC_ECHO_OF_TYRANDE, VISIBLE_RANGE, true);

            if (auraTimer <= diff)
            {
                me->AddAura(SPELL_MOONLANCE_AURA, me);
                auraTimer = -1;
            }
            else
                auraTimer -= diff;

            if (!tyrande)
            {
                me->DespawnOrUnsummon();
                return;
            }

            float x = me->GetPositionX() + (VISIBLE_RANGE * cos(me->GetOrientation()));
            float y = me->GetPositionY() + (VISIBLE_RANGE * sin(me->GetOrientation()));
            float z = me->GetPositionZ();
            me->UpdateAllowedPositionZ(x, y, z);

            me->GetMotionMaster()->MovePoint(0, x, y, z);

            if (timer <= diff)
            {
                me->DespawnOrUnsummon();
            }
            else
                timer -= diff;
        }
    };
};

class npc_eye_of_elune_east : public CreatureScript
{
public:
    npc_eye_of_elune_east() : CreatureScript("npc_eye_of_elune_east") 
    {
    }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_eye_of_elune_eastAI(creature);
    }

    struct npc_eye_of_elune_eastAI : public CreatureAI
    {
        npc_eye_of_elune_eastAI(Creature* creature) : CreatureAI(creature)
        {
            instance = me->GetInstanceScript();
        }
        
        InstanceScript* instance;

        void Reset()
        {
            Unit* tyrande = me->FindNearestCreature(NPC_ECHO_OF_TYRANDE, VISIBLE_RANGE, true);

            if (!tyrande)
            {
                me->DespawnOrUnsummon();
                return;
            }
            
            float orientation = tyrande->GetAngle(me->GetPositionX(), me->GetPositionY());
            float finalOrientation = Position::NormalizeOrientation(orientation + M_PI / 2.f);
            float x = tyrande->GetPositionX() + (EYES_DISTANCE * cos(finalOrientation));
            float y = tyrande->GetPositionY() + (EYES_DISTANCE * sin(finalOrientation));
            float z = tyrande->GetPositionZ();
            me->UpdateAllowedPositionZ(x, y, z);
            me->GetMotionMaster()->MoveJump(x, y, z, EYES_DISTANCE * 5, 0);
            tyrande->AddAura(SPELL_PIERCING_GAZE_OF_ELUNE, me);
        }

        void UpdateAI(uint32 diff)
        {
            Unit* tyrande = me->FindNearestCreature(NPC_ECHO_OF_TYRANDE, VISIBLE_RANGE, true);

            if (!tyrande)
            {
                me->DespawnOrUnsummon();
                return;
            }

            if (!me->isMoving())
            {
                float orientation = tyrande->GetAngle(me->GetPositionX(), me->GetPositionY());
                float finalOrientation = Position::NormalizeOrientation(orientation + M_PI * 2.f / 10.f);
                float x = tyrande->GetPositionX() + (EYES_DISTANCE * cos(finalOrientation));
                float y = tyrande->GetPositionY() + (EYES_DISTANCE * sin(finalOrientation));
                float z = tyrande->GetPositionZ();
                me->GetMotionMaster()->MovePoint(0, x, y, z);
            }
        }
    };
};

class npc_eye_of_elune_west : public CreatureScript
{
public:
    npc_eye_of_elune_west() : CreatureScript("npc_eye_of_elune_west") 
    {
    }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_eye_of_elune_westAI(creature);
    }

    struct npc_eye_of_elune_westAI : public CreatureAI
    {
        npc_eye_of_elune_westAI(Creature* creature) : CreatureAI(creature)
        {
            instance = me->GetInstanceScript();
        }
        
        InstanceScript* instance;

        void Reset()
        {
            Unit* tyrande = me->FindNearestCreature(NPC_ECHO_OF_TYRANDE, VISIBLE_RANGE, true);

            if (!tyrande)
            {
                me->DespawnOrUnsummon();
                return;
            }
            
            float orientation = tyrande->GetAngle(me->GetPositionX(), me->GetPositionY());
            float finalOrientation = Position::NormalizeOrientation(orientation - M_PI / 2.f);
            float x = tyrande->GetPositionX() + (EYES_DISTANCE * cos(finalOrientation));
            float y = tyrande->GetPositionY() + (EYES_DISTANCE * sin(finalOrientation));
            float z = tyrande->GetPositionZ();
            me->UpdateAllowedPositionZ(x, y, z);
            me->GetMotionMaster()->MoveJump(x, y, z, EYES_DISTANCE * 5, 0);
            tyrande->AddAura(SPELL_PIERCING_GAZE_OF_ELUNE, me);
        }

        void UpdateAI(uint32 diff)
        {
            Unit* tyrande = me->FindNearestCreature(NPC_ECHO_OF_TYRANDE, VISIBLE_RANGE, true);

            if (!tyrande)
            {
                me->DespawnOrUnsummon();
                return;
            }

            if (!me->isMoving())
            {
                float orientation = tyrande->GetAngle(me->GetPositionX(), me->GetPositionY());
                float finalOrientation = Position::NormalizeOrientation(orientation + M_PI * 2.f / 10.f);
                float x = tyrande->GetPositionX() + (EYES_DISTANCE * cos(finalOrientation));
                float y = tyrande->GetPositionY() + (EYES_DISTANCE * sin(finalOrientation));
                float z = tyrande->GetPositionZ();
                me->GetMotionMaster()->MovePoint(0, x, y, z);
            }
        }
    };
};

class spell_tears_of_elune : public SpellScriptLoader
{
    public:
        spell_tears_of_elune() : SpellScriptLoader("spell_tears_of_elune") { }

        class spell_tears_of_elune_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_tears_of_elune_SpellScript);

            void HandleScript(SpellEffIndex effIndex)
            {
                if (!urand(0, 3))
                    GetCaster()->CastSpell(GetCaster(), GetSpellInfo()->Effects[effIndex].BasePoints, true);
            }

            void Register()
            {
                OnEffectHitTarget += SpellEffectFn(spell_tears_of_elune_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_tears_of_elune_SpellScript();
        }
};

void AddSC_boss_echo_of_tyrande()
{
    new boss_echo_of_tyrande();
    new npc_moonlace_single();
    new npc_moonlace_divided();
    new npc_eye_of_elune_east();
    new npc_eye_of_elune_west();
    new spell_tears_of_elune();
}
