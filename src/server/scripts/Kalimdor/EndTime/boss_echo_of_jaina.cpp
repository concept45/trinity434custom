/*
 * Copyright (C) 2013-2014 Sovak <sovak007@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "end_time.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"


enum Spells
{
    SPELL_BLINK            = 101812,
    SPELL_FLARECORE        = 101927,
    SPELL_FLARE_TOUCH      = 101980,
    SPELL_FLARE            = 101587,
    SPELL_FROST_BLADES     = 101339,
    SPELL_FROSTBOLT_VOLLEY = 101810,
    SPELL_PYROBLAST        = 101809,
    SPELL_FLARECORE_VISUAL = 101588,
    SPELL_FROSTBLADE       = 101338
};

enum Events
{
    EVENT_PYROBLAST     = 1,
    EVENT_FLARECORE     = 2,
    EVENT_BLINK         = 3,
    EVENT_FROST_BLADES  = 4,
    EVENT_FROST_VOLLEY  = 5
};

enum JainaCreatures
{
    NPC_BLINK_TARGET = 54542,
    NPC_FROSTBLADE   = 54494
};

enum Yells
{
    SAY_AGGRO       = 0,
    SAY_INTRO1      = 1,
    SAY_INTRO2      = 2,
    SAY_SPELL1      = 3,
    SAY_SPELL2      = 4,
    SAY_SPELL3      = 5,
    SAY_SLAY1       = 6,
    SAY_SLAY2       = 7,
    SAY_SLAY3       = 8,
    SAY_DEATH       = 9
};

static const Position JainaSummonPos = {3051.510f, 510.032f, 21.562f, 3.02f};

class boss_echo_of_jaina : public CreatureScript
{
public:
    boss_echo_of_jaina() : CreatureScript("boss_echo_of_jaina") 
    {
    }

    BossAI* GetAI(Creature* creature) const
    {
        return new boss_echo_of_jainaAI(creature);
    }

    struct boss_echo_of_jainaAI : public BossAI
    {
        boss_echo_of_jainaAI(Creature* creature) : BossAI(creature, DATA_ECHO_OF_JAINA)
        {
            instance = me->GetInstanceScript();
            sayIntro = true;
        }

        InstanceScript* instance;
        uint32 pyroblastCount;
        uint32 frostboltVolleyCount;
        bool sayIntro;

        void Reset()
        {
            _Reset();
            events.Reset();
            pyroblastCount = 0;
            frostboltVolleyCount = 0;
            me->SetHealth(me->GetMaxHealth());
        }

        void MoveInLineOfSight(Unit* who)
        {
            if (who->ToPlayer())
            {
                if (me->IsWithinDist(who, 20.f, true) && sayIntro)
                {
                    sayIntro = false;
                    Talk(SAY_AGGRO);
                }
            }
        }

        void EnterEvadeMode()
        {
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_JAINA, FAIL);

            me->GetMotionMaster()->MoveTargetedHome();
            Reset();
        }

        void JustDied(Unit* killer)
        {
            if (instance)
            {
                Talk(SAY_DEATH);
                instance->SetBossState(DATA_ECHO_OF_JAINA, DONE);
            }
        }

        void KilledUnit(Unit* unit)
        {
            Talk(urand(SAY_SLAY1, SAY_SLAY3));
        }

        Unit* SelectTeleportTarget()
        {
            Unit* closest = NULL;
            Unit* closest2 = NULL;
            float dist = 500.f;

            std::list<Creature*> unitList;

            CellCoord p(Trinity::ComputeCellCoord(me->GetPositionX(), me->GetPositionY()));
            Cell cell(p);
            cell.SetNoCreate();

            Trinity::AllCreaturesOfEntryInRange check(me, NPC_BLINK_TARGET, VISIBLE_RANGE);
            Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange> searcher(me, unitList, check);

            TypeContainerVisitor<Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange>, GridTypeMapContainer >  gridGobjectSearcher(searcher);

            cell.Visit(p, gridGobjectSearcher, *me->GetMap(), *me, VISIBLE_RANGE);

            for (std::list<Creature*>::const_iterator iter = unitList.begin(); iter != unitList.end(); iter++)
            {
                if (Creature* target = *iter)
                {
                    if (me->GetDistance2d(target) < dist)
                    {
                        dist = me->GetDistance2d(target);
                        closest2 = closest;
                        closest = target;
                    }
                }
            }

            return closest2 ? closest2 : closest;
        }

        void EnterCombat(Unit* who)
        {
            Talk(SAY_INTRO1);
            pyroblastCount = 0;
            frostboltVolleyCount = 0;
            events.ScheduleEvent(EVENT_PYROBLAST, 1000);
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_JAINA, IN_PROGRESS);
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_PYROBLAST:
                    {
                        if (++pyroblastCount == 3)
                        {
                            events.ScheduleEvent(EVENT_FLARECORE, 500);
                            continue;
                        }
                        if (pyroblastCount == 5)
                        {
                            events.ScheduleEvent(EVENT_BLINK, 1000);
                            continue;
                        }

                        events.ScheduleEvent(EVENT_PYROBLAST, 5000);
                        DoCast(me->GetVictim(), SPELL_PYROBLAST, false);

                        break;           
                    }
                    case EVENT_FLARECORE:
                    {
                        DoCast(me, SPELL_FLARECORE, true);

                        events.ScheduleEvent(EVENT_PYROBLAST, 500);
                        break;
                    }
                    case EVENT_BLINK:
                    {
                        pyroblastCount = 0;
                        DoCast(me, SPELL_BLINK);

                        if (Unit* blink = SelectTeleportTarget())
                            me->NearTeleportTo(blink->GetPositionX(), blink->GetPositionY(), blink->GetPositionZ(), blink->GetOrientation());

                        events.ScheduleEvent(EVENT_FROST_BLADES, 1000);
                        break;
                    }
                    case EVENT_FROST_BLADES:
                    {
                        events.ScheduleEvent(EVENT_FROST_VOLLEY, 1000);
                        DoCast(SPELL_FROST_BLADES);
                        me->SummonCreature(NPC_FROSTBLADE, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation() - 0.35f, TEMPSUMMON_TIMED_DESPAWN, 6000);
                        me->SummonCreature(NPC_FROSTBLADE, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation(), TEMPSUMMON_TIMED_DESPAWN, 6000);
                        me->SummonCreature(NPC_FROSTBLADE, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation() + 0.35f, TEMPSUMMON_TIMED_DESPAWN, 6000);
                        Talk(SAY_SPELL1);
                        break;
                    }
                    case EVENT_FROST_VOLLEY:
                    {
                        if (++frostboltVolleyCount == 2)
                        {
                            events.ScheduleEvent(EVENT_PYROBLAST, 5500);
                            frostboltVolleyCount = 0;
                            pyroblastCount = 0;
                        }
                        else
                            if (frostboltVolleyCount < 2)
                                events.ScheduleEvent(EVENT_FROST_VOLLEY, 5500);
                        DoCast(SPELL_FROSTBOLT_VOLLEY);
                        break;
                    }

                    default:
                        break;
                }
            }
         DoMeleeAttackIfReady();
        }
    };
};

class npc_flarecore : public CreatureScript
{
public:
    npc_flarecore() : CreatureScript("npc_flarecore") 
    {
    }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_flarecoreAI(creature);
    }

    struct npc_flarecoreAI : public CreatureAI
    {
        npc_flarecoreAI(Creature* creature) : CreatureAI(creature)
        {
            instance = me->GetInstanceScript();
        }
        
        InstanceScript* instance;
        uint32 timer;

        void Reset()
        {
            timer = 9000;
            me->AddAura(SPELL_FLARECORE_VISUAL, me);
            me->AddUnitState(UNIT_STATE_ROOT);
        }

        void UpdateAI(uint32 diff)
        {
            if (Player* player = me->SelectNearestPlayer(5.f))
            {
                if (timer < 10000)
                {
                    me->CastSpell(player, SPELL_FLARE_TOUCH, true);
                    me->DespawnOrUnsummon(500);
                    timer = -1;
                }
            }

            if (timer <= diff)
            {
                me->CastSpell(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), SPELL_FLARE, true);
                me->DespawnOrUnsummon(500);
                timer = -1;
            }
            else
                timer -= diff;
        }
    };
};

class spell_echo_of_jaina_frost_blades : public SpellScriptLoader
{
    public:
        spell_echo_of_jaina_frost_blades() : SpellScriptLoader("spell_echo_of_jaina_frost_blades") { }

        class spell_echo_of_jaina_frost_blades_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_echo_of_jaina_frost_blades_SpellScript);

            void HandleSummon(SpellEffIndex effIndex)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;


                GetSpell()->cancel();
            }

            void Register()
            {
                OnEffectHit += SpellEffectFn(spell_echo_of_jaina_frost_blades_SpellScript::HandleSummon, EFFECT_0, SPELL_EFFECT_SUMMON);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_echo_of_jaina_frost_blades_SpellScript();
        }
};


class npc_frost_blade : public CreatureScript
{
public:
    npc_frost_blade() : CreatureScript("npc_frost_blade") 
    {
    }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_frost_bladeAI(creature);
    }

    struct npc_frost_bladeAI : public CreatureAI
    {
        npc_frost_bladeAI(Creature* creature) : CreatureAI(creature)
        {
            instance = me->GetInstanceScript();
        }
        
        InstanceScript* instance;
        uint32 timer;

        void Reset()
        {
            timer = 0;
            me->AddAura(SPELL_FROSTBLADE, me);
            me->SetReactState(REACT_PASSIVE);
        }

        void UpdateAI(uint32 diff)
        {
            float x, y, z;
            me->GetClosePoint(x, y, z, me->GetObjectSize() / 3, 100.0f);
            me->GetMotionMaster()->Clear();
            me->GetMotionMaster()->MovePoint(1, x, y, z, true);

            timer += diff;
            if (timer >= 10000)
                me->DespawnOrUnsummon();
        }
    };
};

void AddSC_boss_echo_of_jaina()
{
    new boss_echo_of_jaina();
    new npc_flarecore();
    new spell_echo_of_jaina_frost_blades();
    new npc_frost_blade();
}
