/*
 * Copyright (C) 2013-2014 Sovak <sovak007@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "end_time.h"

class instance_end_time : public InstanceMapScript
{
    public:
        instance_end_time() : InstanceMapScript("instance_end_time", 938) 
        { 
        }

        InstanceScript* GetInstanceScript(InstanceMap* map) const
        {
            return new instance_end_time_InstanceScript(map);
        }


        struct instance_end_time_InstanceScript : public InstanceScript
        {
            instance_end_time_InstanceScript(InstanceMap* map) : InstanceScript(map)
            {
            }

             uint64 _ghoulGuids[8];
             uint64 _jainaGuid;
             uint32 _hourglass;
             uint32 _staffFragments;
             uint32 activeBoss1;
             uint32 activeBoss2;

            void Initialize()
            {
                activeBoss1 = urand(0, 1);
                activeBoss2 = urand(2, 3);
                _hourglass = 0;
                SetBossNumber(MAX_ENCOUNTER);
                for (int i = 0; i < 8; i++)
                    _ghoulGuids[i] = 0;
                _staffFragments = 0;
            }

            /*void FillInitialWorldStates(WorldPacket& data)
            {
                //data << uint32(WORLDSTATE_SHOW_FRAGMENTS)  << uint32(1);
                //data << uint32(WORLDSTATE_FRAGMENTS_COLLECTED) << uint32(_staffFragments);
            }*/

            void OnCreatureCreate(Creature* creature)
            {
                switch(creature->GetEntry())
                {
                    case BOSS_ECHO_OF_JAINA:
                        _jainaGuid = creature->GetGUID();
                        break;
                    default:
                        break;
                }
            }

            void SetData64(uint32 dataId, uint64 value)
            {
                if (dataId == DATA_FRAGMENTS)
                    _staffFragments = value;

                if (dataId == DATA_HOURGLASS)
                    _hourglass = value;

                if (dataId >= DATA_GHOUL1 && dataId <= DATA_GHOUL8)
                    _ghoulGuids[dataId - DATA_GHOUL1] = value;
            }

            uint64 GetData64(uint32 dataId) const
            {
                if (dataId >= DATA_GHOUL1 && dataId <= DATA_GHOUL8)
                    return _ghoulGuids[dataId - DATA_GHOUL1];

                switch(dataId)
                {
                    case DATA_FRAGMENTS:
                        return _staffFragments;
                    case DATA_HOURGLASS:
                        return _hourglass;
                    case DATA_ECHO_OF_JAINA:
                        return _jainaGuid;
                    case DATA_BOSS1:
                        return activeBoss1;
                    case DATA_BOSS2:
                        return activeBoss2;
                    default:
                        break;
                }
                return 0;
            }

            std::string GetSaveData()
            {
                OUT_SAVE_INST_DATA;

                std::ostringstream saveStream;
                saveStream << "E T " << GetBossSaveData() << activeBoss1 << ' ' << activeBoss2;

                OUT_SAVE_INST_DATA_COMPLETE;
                return saveStream.str();
            }

            void Load(const char* str)
            {
                if (!str)
                {
                    OUT_LOAD_INST_DATA_FAIL;
                    return;
                }

                OUT_LOAD_INST_DATA(str);

                char dataHead1, dataHead2;

                std::istringstream loadStream(str);
                loadStream >> dataHead1 >> dataHead2;

                if (dataHead1 == 'E' && dataHead2 == 'T')
                {
                    for (uint32 i = 0; i < MAX_ENCOUNTER; ++i)
                    {
                        uint32 tmpState;
                        loadStream >> tmpState;
                        if (tmpState == IN_PROGRESS || tmpState > SPECIAL)
                            tmpState = NOT_STARTED;
                        SetBossState(i, EncounterState(tmpState));
                    }

                    loadStream >> activeBoss1;
                    loadStream >> activeBoss2;
                }
                else
                    OUT_LOAD_INST_DATA_FAIL;

                OUT_LOAD_INST_DATA_COMPLETE;
            }
        };
};

void AddSC_instance_end_time()
{
    new instance_end_time();
}