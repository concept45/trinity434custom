/*
 * Copyright (C) 2013-2014 Sovak <sovak007@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "end_time.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "AchievementMgr.h"
#include "Chat.h"
#include "Language.h"
#include "Player.h"
#include "ScriptMgr.h"

enum Spells
{
    SPELL_TEMPORAL_BLAST                        = 102381,
    SPELL_INFINITE_BREATH                       = 102569,
    SPELL_DISTORTION_BOMB                       = 101983,
    SPELL_SANDS_OF_THE_HOURGLASS                = 102668,
    SPELL_DISTORTION_BOMB_AOE                   = 101984,
    SPELL_TEMPORAL_DISPLACEMENT                 = 80354,
    SPELL_WEAKEN_SOUL                           = 6788,
    SPELL_FORBEARENCE                           = 25771,
    SPELL_SATED                                 = 57724,
    SPELL_EXHAUSTION                            = 57723,
    SPELL_BLESSING_OF_THE_BRONZE_DRAGONFLIGHT   = 102364
};

enum Events
{
    EVENT_TEMPORAL_BLAST    = 1,
    EVENT_INFINITE_BREATH   = 2,
    EVENT_DISTORTION_BOMB   = 3
};

enum NPCs
{
    BOSS_MUROZOND = 54432
};

enum CreatureText
{
    SAY_INTRO1  = 0,
    SAY_INTRO2  = 1,
    SAY_AGGRO   = 2,
    SAY_EVENT1  = 3,
    SAY_EVENT2  = 4,
    SAY_EVENT3  = 5,
    SAY_EVENT4  = 6,
    SAY_EVENT5  = 7,
    SAY_SLAY1   = 8,
    SAY_SLAY2   = 9,
    SAY_SLAY3   = 10,
    SAY_DEATH   = 11
};

class boss_murozond : public CreatureScript
{
public:
    boss_murozond() : CreatureScript("boss_murozond") 
    {
    }

    BossAI* GetAI(Creature* creature) const
    {
        return new boss_murozondAI(creature);
    }

    struct boss_murozondAI : public BossAI
    {
        boss_murozondAI(Creature* creature) : BossAI(creature, DATA_MUROZOND)
        {
            instance = me->GetInstanceScript();
        }

        InstanceScript* instance;
        uint32 timer;
        bool saidIntro;

        void Reset()
        {
            events.Reset();
            me->RemoveAllAuras();
            instance->SetData64(DATA_HOURGLASS, 5);
            timer = 300;
            saidIntro = false;

            for (int i = 0; i < 500; i++)
            {
                if (DynamicObject* dobject = me->GetDynObject(SPELL_DISTORTION_BOMB_AOE))
                {
                    dobject->Remove();
                }
                else break;
            }
        }

        void EnterEvadeMode()
        {
            if (instance)
                instance->SetBossState(DATA_MUROZOND, FAIL);

            Map::PlayerList const &playerList = me->GetMap()->GetPlayers();
            for (Map::PlayerList::const_iterator iter = playerList.begin(); iter != playerList.end(); ++iter)
            {
                if (Player* player = iter->GetSource())
                {
                    if (player->IsAlive())
                    {
                        if (player->HasAura(SPELL_SANDS_OF_THE_HOURGLASS))
                        {
                            player->RemoveAurasDueToSpell(SPELL_SANDS_OF_THE_HOURGLASS);
                        }
                    }
                }
            }
            me->GetMotionMaster()->MoveTargetedHome();
            Reset();
        }

        void KilledUnit(Unit* victim)
        {
            Talk(urand(SAY_SLAY1, SAY_SLAY3));
        }

        void JustDied(Unit* killer)
        {
			std::list<Player*> TargetList;
			Map::PlayerList const& Players = me->GetMap()->GetPlayers();
			for (Map::PlayerList::const_iterator itr = Players.begin(); itr != Players.end(); ++itr)
			{	
				if (Player* player = itr->GetSource())
				{
					if (AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(6117))
						if(IsHeroic())
							player->CompletedAchievement(achievementEntry);
				}	
			}
			
            for (int i = 0; i < 500; i++)
            {
                if (DynamicObject* dobject = me->GetDynObject(SPELL_DISTORTION_BOMB_AOE))
                {
                    dobject->Remove();
                }
                else break;
            }
			
            if (instance)
                instance->SetBossState(DATA_MUROZOND, DONE);

            Talk(SAY_DEATH);

            Map::PlayerList const &playerList = me->GetMap()->GetPlayers();
            for (Map::PlayerList::const_iterator iter = playerList.begin(); iter != playerList.end(); ++iter)
            {
                if (Player* player = iter->GetSource())
                {
                    if (player->IsAlive())
                    {
                        if (player->HasAura(SPELL_BLESSING_OF_THE_BRONZE_DRAGONFLIGHT))
                            player->RemoveAurasDueToSpell(SPELL_BLESSING_OF_THE_BRONZE_DRAGONFLIGHT);

                        if (player->HasAura(SPELL_SANDS_OF_THE_HOURGLASS))
                            player->RemoveAurasDueToSpell(SPELL_SANDS_OF_THE_HOURGLASS);
                    }
                }
            }
        }

        void EnterCombat(Unit* who)
        {
            if (instance)
                instance->SetBossState(DATA_MUROZOND, IN_PROGRESS);

            Talk(SAY_AGGRO);
            events.ScheduleEvent(EVENT_TEMPORAL_BLAST, 12000);
            events.ScheduleEvent(EVENT_INFINITE_BREATH, 22000);
            events.ScheduleEvent(EVENT_DISTORTION_BOMB, 15000);
        }

        void MoveInLineOfSight(Unit* who)
        {
            if (who->ToPlayer())
            {
                if (me->IsWithinDist(who, 50.f, true) && !saidIntro)
                {
                    saidIntro = true;
                    Talk(urand(SAY_INTRO1, SAY_INTRO2));
                }
            }
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            if (timer <= diff)
            {
                Map::PlayerList const &playerList = me->GetMap()->GetPlayers();
                for (Map::PlayerList::const_iterator iter = playerList.begin(); iter != playerList.end(); ++iter)
                {
                    if (Player* player = iter->GetSource())
                    {
                        if (player->IsAlive())
                        {
                            if (!player->HasAura(SPELL_BLESSING_OF_THE_BRONZE_DRAGONFLIGHT))
                                player->AddAura(SPELL_BLESSING_OF_THE_BRONZE_DRAGONFLIGHT, player);

                            player->SetMaxPower(POWER_ALTERNATE_POWER, 5);
                            if (instance->GetData64(DATA_HOURGLASS))
                            {
                                if (player->HasAura(SPELL_SANDS_OF_THE_HOURGLASS))
                                {
                                    if (player->GetPower(POWER_ALTERNATE_POWER) != instance->GetData64(DATA_HOURGLASS))
                                        player->SetPower(POWER_ALTERNATE_POWER, instance->GetData64(DATA_HOURGLASS));
                                }
                                else
                                {
                                        me->AddAura(SPELL_SANDS_OF_THE_HOURGLASS, player);
                                        player->SetPower(POWER_ALTERNATE_POWER, instance->GetData64(DATA_HOURGLASS));
                                }
                            }
                            else
                                if (player->HasAura(SPELL_SANDS_OF_THE_HOURGLASS))
                                    player->RemoveAurasDueToSpell(SPELL_SANDS_OF_THE_HOURGLASS);
                        }
                    }
                }
                timer = 300;
            }
            else
                timer -= diff;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_TEMPORAL_BLAST:
                    {
                        DoCast(SPELL_TEMPORAL_BLAST);
                        events.ScheduleEvent(EVENT_TEMPORAL_BLAST, 12000);
                        break;
                    }
                    case EVENT_INFINITE_BREATH:
                    {
                        DoCast(SPELL_INFINITE_BREATH);
                        events.ScheduleEvent(SPELL_INFINITE_BREATH, 22000);
                        break;
                    }
                    case EVENT_DISTORTION_BOMB:
                    {
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, VISIBLE_RANGE, true))
                            DoCast(target, SPELL_DISTORTION_BOMB);
                        events.ScheduleEvent(EVENT_DISTORTION_BOMB, 15000);
                        break;
                        
                    }
                }
            }

         DoMeleeAttackIfReady();
        }
    };
};


class go_hourglass : public GameObjectScript
{
    public:
        go_hourglass() : GameObjectScript("go_hourglass") { }

        bool OnGossipHello(Player* player, GameObject* go)
        {
            InstanceScript* instance = player->GetInstanceScript();
            if (!instance)
                return true;

            if (instance->GetBossState(DATA_MUROZOND) != IN_PROGRESS)
                return true;

            if (!instance->GetData64(DATA_HOURGLASS))
                return true;

            if (player->HasAura(101591))
                return true;

            instance->SetData64(DATA_HOURGLASS, instance->GetData64(DATA_HOURGLASS) - 1);

            Map::PlayerList const &playerList = player->GetMap()->GetPlayers();
            Unit* boss = player->FindNearestCreature(BOSS_MUROZOND, 5000.f, true);
            if (boss)
                ((CreatureAI*)(boss->GetAI()))->Talk(SAY_EVENT1 + 4 - instance->GetData64(DATA_HOURGLASS));

            for (Map::PlayerList::const_iterator iter = playerList.begin(); iter != playerList.end(); ++iter)
            {
                if (Player* plr = iter->GetSource())
                {
                    if (plr->IsAlive())
                    {
                        plr->SetFullHealth();
                        plr->SetMaxPower(POWER_ALTERNATE_POWER, 5);
                        plr->SetPower(POWER_MANA, plr->GetMaxPower(POWER_MANA));
                        plr->SetPower(POWER_ALTERNATE_POWER, instance->GetData64(DATA_HOURGLASS));
                        plr->RemoveAllSpellCooldown();
                        plr->RemoveAurasDueToSpell(SPELL_TEMPORAL_DISPLACEMENT);
                        plr->RemoveAurasDueToSpell(SPELL_WEAKEN_SOUL);
                        plr->RemoveAurasDueToSpell(SPELL_FORBEARENCE);
                        plr->RemoveAurasDueToSpell(SPELL_EXHAUSTION);
                        plr->RemoveAurasDueToSpell(SPELL_SATED);
                        plr->RemoveAurasDueToSpell(SPELL_TEMPORAL_BLAST);
                        if (boss)
                        {
                            for (int i = 0; i < 500; i++)
                            {
                                if (DynamicObject* dobject = boss->GetDynObject(SPELL_DISTORTION_BOMB_AOE))
                                {
                                    dobject->Remove();
                                }
                                else break;
                            }
                        }
                    }
                }
            }
            return false;
        }
};

void AddSC_boss_murozond()
{
    new go_hourglass();
    new boss_murozond();
}
