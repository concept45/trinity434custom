/*
 * Copyright (C) 2005 - 2011 MaNGOS <http://www.getmangos.org/>
 *
 * Copyright (C) 2008 - 2011 TrinityCore <http://www.trinitycore.org/>
 *
 * Copyright (C) 2011 - 2012 ArkCORE <http://www.arkania.net/>
 *
 * Copyright (C) 2011 - 2012 Forgotten Lands <http://www.forgottenlands.eu>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* Script SQLs

DELETE FROM `creature_template`  WHERE `entry` IN (51350, 47120);
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `exp_unk`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `type_flags2`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Mana_mod_extra`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES
(47120, 51350, 0, 0, 0, 0, 35426, 0, 0, 0, 'Argaloth', '', '', 0, 88, 88, 3, 0, 16, 16, 0, 1.2, 1.71429, 1, 1, 0, 0, 0, 0, 1, 1500, 2000, 1, 0, 134219776, 0, 0, 0, 0, 0, 0, 0, 0, 3, 108, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 250, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 164, 1, 617291772, 1, '', 15595),
(51350, 0, 0, 0, 0, 0, 35426, 0, 0, 0, 'Argaloth (1)', '', '', 0, 88, 88, 3, 0, 16, 16, 0, 1, 1.14286, 1, 1, 0, 0, 0, 0, 1, 1500, 2000, 1, 0, 2048, 0, 0, 0, 0, 0, 0, 0, 0, 3, 108, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 750, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 164, 1, 617291772, 1, '', 15595);

UPDATE `creature_template` SET `ScriptName`='boss_argaloth' WHERE  `entry`=47120 LIMIT 1;
UPDATE `creature_template` SET `ScriptName`='boss_argaloth' WHERE  `entry`=51350 LIMIT 1;

DELETE FROM `creature_onkill_currency` WHERE `creature_id` IN (47120, 51350);
INSERT INTO `creature_onkill_currency` (`creature_id`, `CurrencyId1`, `CurrencyCount1`) VALUES
(47120, 396, 40),
(51350, 396, 40);

UPDATE `creature_template` SET `modelid1`=11686, `modelid2`=0, `minlevel`=85, `maxlevel`=85, `faction_A`=16, `faction_H`=16 WHERE  `entry`=47829 LIMIT 1;

DELETE FROM `creature_template_addon` WHERE `entry` = 47829;
INSERT INTO `creature_template_addon` (`entry`, `auras`) VALUES
(47829, '88999');

UPDATE `creature_template` SET `flags_extra`=2 WHERE  `entry`=47829 LIMIT 1;
UPDATE `creature_template` SET `unit_flags`=33554438 WHERE  `entry`=47829 LIMIT 1;

*/

#include"ScriptPCH.h"
#include"baradin_hold.h"

enum Spells
{
    SPELL_BERSERK                                 = 47008,
    SPELL_CONSUMING_DARKNESS                      = 88954,
    SPELL_METEOR_SLASH                            = 88942,
    SPELL_FEL_FIRESTORM                           = 88972,
};

enum Events
{
    EVENT_BERSERK = 1,
    EVENT_CONSUMING_DARKNESS,
    EVENT_METEOR_SLASH,
    EVENT_FLAME_DESPAWN,
};

class boss_argaloth: public CreatureScript
{
    public:
        boss_argaloth() : CreatureScript("boss_argaloth") { }

    struct boss_argalothAI: public BossAI
    {
        boss_argalothAI(Creature* creature) : BossAI(creature, DATA_ARGALOTH_EVENT), summons(me) 
        {
            instance = me->GetInstanceScript();
        }

        uint32 fel_firestorm_casted;
        SummonList summons;
        InstanceScript* instance;

        void Reset()
        {
            _Reset();
            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            events.Reset();
            fel_firestorm_casted = 0;
            summons.DespawnAll();

            if (instance)
                instance->SetBossState(DATA_ARGALOTH_EVENT, NOT_STARTED);
        }

        void JustDied(Unit* killer)
        {
            if (instance)
                instance->SetBossState(DATA_ARGALOTH_EVENT, DONE);
        }

        void EnterCombat(Unit* who)
        {
            events.ScheduleEvent(EVENT_BERSERK, 300000);
            events.ScheduleEvent(EVENT_CONSUMING_DARKNESS, urand(14000, 16000));
            events.ScheduleEvent(EVENT_METEOR_SLASH, urand(16000, 18000));

            if (instance)
                instance->SetBossState(DATA_ARGALOTH_EVENT, IN_PROGRESS);
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            if (me->GetHealthPct() < 66 && fel_firestorm_casted == 0)
            {
                DoCast(SPELL_FEL_FIRESTORM);
                events.DelayEvents(3000);
                fel_firestorm_casted = 1;
                events.ScheduleEvent(EVENT_FLAME_DESPAWN, 5000);
            }
            if (me->GetHealthPct() < 33 && fel_firestorm_casted == 1)
            {
                DoCast(SPELL_FEL_FIRESTORM);
                events.DelayEvents(3000);
                fel_firestorm_casted = 2;
                events.ScheduleEvent(EVENT_FLAME_DESPAWN, 5000);
            }

            events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_CONSUMING_DARKNESS:
                        for (uint8 i = 0; i < RAID_MODE(3, 8); i++)
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true, -SPELL_CONSUMING_DARKNESS))
                                me->AddAura(SPELL_CONSUMING_DARKNESS, target);

                        events.RescheduleEvent(EVENT_CONSUMING_DARKNESS, urand(24000, 26000));
                        break;
                    case EVENT_METEOR_SLASH:
                        me->CastSpell(me, 88949, true);
                        DoCast(SPELL_METEOR_SLASH);
                        events.RescheduleEvent(EVENT_METEOR_SLASH, urand(16000, 18000));
                        break;
                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                    case EVENT_FLAME_DESPAWN:
                        summons.DespawnAll();
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }

        void JustSummoned(Creature* summoned)
        {
            summons.Summon(summoned);
        }

     };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_argalothAI(creature);
    }
};

void AddSC_boss_argaloth()
{
    new boss_argaloth();
}