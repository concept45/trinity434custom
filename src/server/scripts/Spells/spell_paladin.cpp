/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Scripts for spells with SPELLFAMILY_PALADIN and SPELLFAMILY_GENERIC spells used by paladin players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_pal_".
 */

#include "Player.h"
#include "ScriptMgr.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "Group.h"

enum PaladinSpells
{
    //Spells related to Guardian of ancient kings
    SPELL_PALADIN_GUARDIAN_ANCIENT_KINGS         = 86150, //Dummy
    SPELL_PALADIN_GUARDIAN_RETRIBUTION           = 86698,
    SPELL_PALADIN_GUARDIAN_HOLY                  = 86669,
    SPELL_PALADIN_GUARDIAN_PROTECTION            = 86659,

    SPELL_PALADIN_AVENGERS_SHIELD               = 31935,
    SPELL_PALADIN_AURA_MASTERY_IMMUNE            = 64364,
    SPELL_PALADIN_BEACON_OF_LIGHT_MARKER         = 53563,
	SPELL_PALADIN_BEACON_OF_LIGHT                = 53563,
    SPELL_PALADIN_BEACON_OF_LIGHT_HEAL           = 53652,
	SPELL_PALADIN_TOWER_OF_RADIANCE              = 88852,
    SPELL_PALADIN_BLESSING_OF_LOWER_CITY_DRUID   = 37878,
    SPELL_PALADIN_BLESSING_OF_LOWER_CITY_PALADIN = 37879,
    SPELL_PALADIN_BLESSING_OF_LOWER_CITY_PRIEST  = 37880,
    SPELL_PALADIN_BLESSING_OF_LOWER_CITY_SHAMAN  = 37881,
    SPELL_PALADIN_CONCENTRACTION_AURA            = 19746,
    SPELL_PALADIN_DIVINE_PURPOSE_PROC            = 90174,
    SPELL_PALADIN_DIVINE_SACRIFICE               = 64205,
    SPELL_PALADIN_DIVINE_STORM                   = 53385,
    SPELL_PALADIN_DIVINE_STORM_DUMMY             = 54171,
    SPELL_PALADIN_DIVINE_STORM_HEAL              = 54172,
    SPELL_PALADIN_EYE_FOR_AN_EYE_RANK_1          = 9799,
    SPELL_PALADIN_EYE_FOR_AN_EYE_DAMAGE          = 25997,
    SPELL_PALADIN_FORBEARANCE                    = 25771,
    SPELL_PALADIN_GLYPH_OF_SALVATION             = 63225,
    SPELL_PALADIN_HAND_OF_SACRIFICE              = 6940,
    SPELL_PALADIN_HOLY_LIGHT                     = 635,
    SPELL_PALADIN_HOLY_SHOCK_R1                  = 20473,
    SPELL_PALADIN_HOLY_SHOCK_R1_DAMAGE           = 25912,
    SPELL_PALADIN_HOLY_SHOCK_R1_HEALING          = 25914,
    SPELL_PALADIN_IMMUNE_SHIELD_MARKER           = 61988,
    SPELL_PALADIN_IMPROVED_CONCENTRACTION_AURA   = 63510,
    SPELL_PALADIN_IMPROVED_DEVOTION_AURA         = 63514,
    SPELL_PALADIN_ITEM_HEALING_TRANCE            = 37706,
    SPELL_PALADIN_JUDGEMENT_DAMAGE               = 54158, 
    SPELL_PALADIN_RIGHTEOUS_DEFENSE_TAUNT        = 31790,
    SPELL_PALADIN_SANCTIFIED_RETRIBUTION_AURA    = 63531,
    SPELL_PALADIN_SANCTIFIED_RETRIBUTION_R1      = 31869,
    SPELL_PALADIN_SEAL_OF_JUSTICE                = 20164, 
    SPELL_PALADIN_SEAL_OF_RIGHTEOUSNESS          = 25742,
    SPELL_PALADIN_SWIFT_RETRIBUTION_R1           = 53379,
    SPELL_PALADIN_SEAL_OF_INSIGHT                = 20167,
    SPELL_PALADIN_SACRED_SHIELD_TRIGGERED        = 96263,
	SPELL_PALA_DIVINEPOR_R1						 = 85117,
	SPELL_PALA_DIVINEPOR_R2						 = 86172,
	SPELL_PALADIN_CS_R1							 = 53375,
	SPELL_PALADIN_CS_R2							 = 90286,
	SPELL_PALADIN_CS_R3							 = 53376,
	SPELL_PALADIN_CS_AURA						 = 57318,
	SPELL_PALADIN_SPEED_OF_LIGHT				 = 85497,
    SPELL_PALADIN_GUARDED_BY_THE_LIGHT_RANK_2    = 85646,
    SPELL_PALADIN_GUARDED_BY_THE_LIGHT_SHIELD    = 88063,	
	
};

enum MiscSpells
{
    SPELL_GENERIC_ARENA_DAMPENING                = 74410,
    SPELL_GENERIC_BATTLEGROUND_DAMPENING         = 74411
};

enum PaladinSpellIcons
{
    PALADIN_ICON_ID_RETRIBUTION_AURA             = 555
};

// 31850 - Ardent Defender
class spell_pal_ardent_defender : public SpellScriptLoader
{
    public:
        spell_pal_ardent_defender() : SpellScriptLoader("spell_pal_ardent_defender") { }

        class spell_pal_ardent_defender_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_ardent_defender_AuraScript);

            uint32 absorbPct, healPct;

            enum Spell
            {
                PAL_SPELL_ARDENT_DEFENDER_HEAL = 66235,
            };

            bool Load()
            {
                healPct = GetSpellInfo()->Effects[EFFECT_1].CalcValue();
                absorbPct = GetSpellInfo()->Effects[EFFECT_0].CalcValue();
                return GetUnitOwner()->GetTypeId() == TYPEID_PLAYER;
            }

            void CalculateAmount(AuraEffect const* aurEff, int32 & amount, bool & canBeRecalculated)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* aurEff, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                if(Unit* victim = GetTarget())
                {
                    uint32 allowedHealth = victim->CountPctFromMaxHealth(100);
                    int32 remainingHealth = victim->GetHealth() - dmgInfo.GetDamage();

                    if (remainingHealth <= 0 && !victim->ToPlayer()->HasSpellCooldown(PAL_SPELL_ARDENT_DEFENDER_HEAL))
                    {
                        int32 healAmount = int32(victim->CountPctFromMaxHealth(15));
                        victim->CastCustomSpell(victim, PAL_SPELL_ARDENT_DEFENDER_HEAL, &healAmount, NULL, NULL, true, NULL, aurEff);
                    }
                    else
                    {
                        uint32 damageToReduce = (victim->GetHealth() <= allowedHealth)
                            ? dmgInfo.GetDamage()
                            : allowedHealth - remainingHealth;
                        absorbAmount = CalculatePct(damageToReduce, absorbPct);
                    }
                }
            }

            void Register()
            {
                 DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_ardent_defender_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                 OnEffectAbsorb += AuraEffectAbsorbFn(spell_pal_ardent_defender_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pal_ardent_defender_AuraScript();
        }
};


// 31821 - Aura Mastery
class spell_pal_aura_mastery : public SpellScriptLoader
{
    public:
        spell_pal_aura_mastery() : SpellScriptLoader("spell_pal_aura_mastery") { }

        class spell_pal_aura_mastery_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_aura_mastery_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_AURA_MASTERY_IMMUNE))
                    return false;
                return true;
            }

            void HandleEffectApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_AURA_MASTERY_IMMUNE, true);
            }

            void HandleEffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                GetTarget()->RemoveOwnedAura(SPELL_PALADIN_AURA_MASTERY_IMMUNE, GetCasterGUID());
            }

            void Register() OVERRIDE
            {
                AfterEffectApply += AuraEffectApplyFn(spell_pal_aura_mastery_AuraScript::HandleEffectApply, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
                AfterEffectRemove += AuraEffectRemoveFn(spell_pal_aura_mastery_AuraScript::HandleEffectRemove, EFFECT_0, SPELL_AURA_ADD_PCT_MODIFIER, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_aura_mastery_AuraScript();
        }
};

// 64364 - Aura Mastery Immune
class spell_pal_aura_mastery_immune : public SpellScriptLoader
{
    public:
        spell_pal_aura_mastery_immune() : SpellScriptLoader("spell_pal_aura_mastery_immune") { }

        class spell_pal_aura_mastery_immune_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_aura_mastery_immune_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_CONCENTRACTION_AURA))
                    return false;
                return true;
            }

            bool CheckAreaTarget(Unit* target)
            {
                return target->HasAura(SPELL_PALADIN_CONCENTRACTION_AURA, GetCasterGUID());
            }

            void Register() OVERRIDE
            {
                DoCheckAreaTarget += AuraCheckAreaTargetFn(spell_pal_aura_mastery_immune_AuraScript::CheckAreaTarget);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_aura_mastery_immune_AuraScript();
        }
};

// 53651 - Light's Beacon - Beacon of Light
class spell_pal_lights_beacon : public SpellScriptLoader
{
    public:
        spell_pal_lights_beacon() : SpellScriptLoader("spell_pal_lights_beacon") { }

        class spell_pal_lights_beacon_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_lights_beacon_AuraScript);

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* beaconOwner = GetCaster();
                Unit* healTarget = GetTarget();
                Unit* paladinMaster = eventInfo.GetProcTarget();

                if (!beaconOwner || !healTarget || !paladinMaster)
                    return;

                //Check if it was heal by paladin which casted this beacon of light
                if (beaconOwner->GetAura(SPELL_PALADIN_BEACON_OF_LIGHT, paladinMaster->GetGUID()))
                {
                    if (beaconOwner->IsWithinLOSInMap(paladinMaster))
                    {
                        int32 mod = 100;
                        bool towerOfRadiance = false;
                        
                        switch (eventInfo.GetDamageInfo()->GetSpellInfo()->Id)
                        {
                            case 19750: // Flash of Light
                            case 82326: // Divine Light
                                towerOfRadiance = true;
                            case 85673: // Word of Glory
                            case 25914: // Holy Shock
                            case 85222: // Light of Dawn
                                mod = 50; // 50% heal from these spells
                                break;
                            case 635:   // Holy Light
                                mod = 100; // 100% heal from Holy Light
                                break;
                            default:
                                return;
                        }

                        // False when target of heal is beaconed
                        if (beaconOwner == healTarget)
                        {
                            // Tower of Radiance talent
                            if(towerOfRadiance)
                                if(AuraEffect* aurEff = paladinMaster->GetDummyAuraEffect(SPELLFAMILY_PALADIN, 3402, EFFECT_0))
                                    if(roll_chance_i(aurEff->GetAmount()))
                                        paladinMaster->CastSpell(paladinMaster, SPELL_PALADIN_TOWER_OF_RADIANCE, true);
                            return;
                        }

                        int32 basepoints0 = CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), mod);

                        paladinMaster->CastCustomSpell(beaconOwner, SPELL_PALADIN_BEACON_OF_LIGHT_HEAL, &basepoints0, NULL, NULL, true);
                    }
                }
            }

            void Register()
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_lights_beacon_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pal_lights_beacon_AuraScript();
        }
};

// 37877 - Blessing of Faith
class spell_pal_blessing_of_faith : public SpellScriptLoader
{
    public:
        spell_pal_blessing_of_faith() : SpellScriptLoader("spell_pal_blessing_of_faith") { }

        class spell_pal_blessing_of_faith_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_blessing_of_faith_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_BLESSING_OF_LOWER_CITY_DRUID) || !sSpellMgr->GetSpellInfo(SPELL_PALADIN_BLESSING_OF_LOWER_CITY_PALADIN) || !sSpellMgr->GetSpellInfo(SPELL_PALADIN_BLESSING_OF_LOWER_CITY_PRIEST) || !sSpellMgr->GetSpellInfo(SPELL_PALADIN_BLESSING_OF_LOWER_CITY_SHAMAN))
                    return false;
                return true;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* unitTarget = GetHitUnit())
                {
                    uint32 spell_id = 0;
                    switch (unitTarget->getClass())
                    {
                        case CLASS_DRUID:
                            spell_id = SPELL_PALADIN_BLESSING_OF_LOWER_CITY_DRUID;
                            break;
                        case CLASS_PALADIN:
                            spell_id = SPELL_PALADIN_BLESSING_OF_LOWER_CITY_PALADIN;
                            break;
                        case CLASS_PRIEST:
                            spell_id = SPELL_PALADIN_BLESSING_OF_LOWER_CITY_PRIEST;
                            break;
                        case CLASS_SHAMAN:
                            spell_id = SPELL_PALADIN_BLESSING_OF_LOWER_CITY_SHAMAN;
                            break;
                        default:
                            return; // ignore for non-healing classes
                    }
                    Unit* caster = GetCaster();
                    caster->CastSpell(caster, spell_id, true);
                }
            }

            void Register() OVERRIDE
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_blessing_of_faith_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_blessing_of_faith_SpellScript();
        }
};

// 64205 - Divine Sacrifice
class spell_pal_divine_sacrifice : public SpellScriptLoader
{
    public:
        spell_pal_divine_sacrifice() : SpellScriptLoader("spell_pal_divine_sacrifice") { }

        class spell_pal_divine_sacrifice_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_divine_sacrifice_AuraScript);

            uint32 groupSize, minHpPct;
            int32 remainingAmount;

            bool Load() OVERRIDE
            {

                if (Unit* caster = GetCaster())
                {
                    if (caster->GetTypeId() == TYPEID_PLAYER)
                    {
                        if (caster->ToPlayer()->GetGroup())
                            groupSize = caster->ToPlayer()->GetGroup()->GetMembersCount();
                        else
                            groupSize = 1;
                    }
                    else
                        return false;

                    remainingAmount = (caster->CountPctFromMaxHealth(GetSpellInfo()->Effects[EFFECT_2].CalcValue(caster)) * groupSize);
                    minHpPct = GetSpellInfo()->Effects[EFFECT_1].CalcValue(caster);
                    return true;
                }
                return false;
            }

            void Split(AuraEffect* /*aurEff*/, DamageInfo & /*dmgInfo*/, uint32 & splitAmount)
            {
                remainingAmount -= splitAmount;
                // break when absorbed everything it could, or if the casters hp drops below 20%
                if (Unit* caster = GetCaster())
                    if (remainingAmount <= 0 || (caster->GetHealthPct() < minHpPct))
                        caster->RemoveAura(SPELL_PALADIN_DIVINE_SACRIFICE);
            }

            void Register() OVERRIDE
            {
                OnEffectSplit += AuraEffectSplitFn(spell_pal_divine_sacrifice_AuraScript::Split, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_divine_sacrifice_AuraScript();
        }
};

// 53385 - Divine Storm
class spell_pal_divine_storm : public SpellScriptLoader
{
    public:
        spell_pal_divine_storm() : SpellScriptLoader("spell_pal_divine_storm") { }

        class spell_pal_divine_storm_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_divine_storm_SpellScript);

            uint32 healPct;

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_DIVINE_STORM_DUMMY))
                    return false;
                return true;
            }

            bool Load() OVERRIDE
            {
                healPct = GetSpellInfo()->Effects[EFFECT_1].CalcValue(GetCaster());
                return true;
            }

            void TriggerHeal()
            {
                Unit* caster = GetCaster();
                caster->CastCustomSpell(SPELL_PALADIN_DIVINE_STORM_DUMMY, SPELLVALUE_BASE_POINT0, (GetHitDamage() * healPct) / 100, caster, true);
            }

            void Register() OVERRIDE
            {
                AfterHit += SpellHitFn(spell_pal_divine_storm_SpellScript::TriggerHeal);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_divine_storm_SpellScript();
        }
};

// 54171 - Divine Storm (Dummy)
class spell_pal_divine_storm_dummy : public SpellScriptLoader
{
    public:
        spell_pal_divine_storm_dummy() : SpellScriptLoader("spell_pal_divine_storm_dummy") { }

        class spell_pal_divine_storm_dummy_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_divine_storm_dummy_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_DIVINE_STORM_HEAL))
                    return false;
                return true;
            }

            void CountTargets(std::list<WorldObject*>& targetList)
            {
                _targetCount = targetList.size();
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (!_targetCount || ! GetHitUnit())
                    return;

                int32 heal = GetEffectValue() / _targetCount;
                GetCaster()->CastCustomSpell(GetHitUnit(), SPELL_PALADIN_DIVINE_STORM_HEAL, &heal, NULL, NULL, true);
            }
        private:
            uint32 _targetCount;

            void Register() OVERRIDE
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_divine_storm_dummy_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_divine_storm_dummy_SpellScript::CountTargets, EFFECT_0, TARGET_UNIT_CASTER_AREA_RAID);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_divine_storm_dummy_SpellScript();
        }
};

// 33695 - Exorcism and Holy Wrath Damage
class spell_pal_exorcism_and_holy_wrath_damage : public SpellScriptLoader
{
    public:
        spell_pal_exorcism_and_holy_wrath_damage() : SpellScriptLoader("spell_pal_exorcism_and_holy_wrath_damage") { }

        class spell_pal_exorcism_and_holy_wrath_damage_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_exorcism_and_holy_wrath_damage_AuraScript);

            void HandleEffectCalcSpellMod(AuraEffect const* aurEff, SpellModifier*& spellMod)
            {
                if (!spellMod)
                {
                    spellMod = new SpellModifier(aurEff->GetBase());
                    spellMod->op = SPELLMOD_DAMAGE;
                    spellMod->type = SPELLMOD_FLAT;
                    spellMod->spellId = GetId();
                    spellMod->mask[1] = 0x200002;
                }

                spellMod->value = aurEff->GetAmount();
            }

            void Register() OVERRIDE
            {
                DoEffectCalcSpellMod += AuraEffectCalcSpellModFn(spell_pal_exorcism_and_holy_wrath_damage_AuraScript::HandleEffectCalcSpellMod, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_exorcism_and_holy_wrath_damage_AuraScript();
        }
};

// -75806 - Grand Crusader
class spell_pal_grand_crusader : public SpellScriptLoader
{
    public:
        spell_pal_grand_crusader() : SpellScriptLoader("spell_pal_grand_crusader") { }

        class spell_pal_grand_crusader_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_grand_crusader_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_AVENGERS_SHIELD))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                return GetTarget()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                GetTarget()->ToPlayer()->RemoveSpellCooldown(SPELL_PALADIN_AVENGERS_SHIELD, true);
            }

            void Register() OVERRIDE
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_grand_crusader_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_grand_crusader_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_grand_crusader_AuraScript();
        }
};

// 9799/25988  - Eye for an Eye
class spell_pal_eye_for_an_eye : public SpellScriptLoader
{
    public:
        spell_pal_eye_for_an_eye() : SpellScriptLoader("spell_pal_eye_for_an_eye") { }

        class spell_pal_eye_for_an_eye_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_eye_for_an_eye_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_EYE_FOR_AN_EYE_DAMAGE))
                    return false;
                return true;
            }

            void OnProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                if(Unit* target = GetTarget())
                {
                    if(Unit* procTarget = eventInfo.GetProcTarget())
                    {
                        if(eventInfo.GetDamageInfo())
                        {
                            if(uint32 procDamage = eventInfo.GetDamageInfo()->GetDamage())
                            {
                                // return damage % to attacker but < 50% own total health
                                int32 damage = int32(std::min(CalculatePct(procDamage, aurEff->GetAmount()), target->GetMaxHealth() / 2));
                                target->CastCustomSpell(SPELL_PALADIN_EYE_FOR_AN_EYE_DAMAGE, SPELLVALUE_BASE_POINT0, damage, procTarget, true, NULL, aurEff);
                            }
                        }
                    }
                }
            }

            void Register()
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_eye_for_an_eye_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectProc += AuraEffectProcFn(spell_pal_eye_for_an_eye_AuraScript::OnProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pal_eye_for_an_eye_AuraScript();
        }
};

// 6940 - Hand of Sacrifice
class spell_pal_hand_of_sacrifice : public SpellScriptLoader
{
    public:
        spell_pal_hand_of_sacrifice() : SpellScriptLoader("spell_pal_hand_of_sacrifice") { }

        class spell_pal_hand_of_sacrifice_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_hand_of_sacrifice_AuraScript);

            int32 remainingAmount;

            bool Load() OVERRIDE
            {
                if (Unit* caster = GetCaster())
                {
                    remainingAmount = caster->GetMaxHealth();
                    return true;
                }
                return false;
            }

            void Split(AuraEffect* /*aurEff*/, DamageInfo & /*dmgInfo*/, uint32 & splitAmount)
            {
                remainingAmount -= splitAmount;

                if (remainingAmount <= 0)
                {
                    GetTarget()->RemoveAura(SPELL_PALADIN_HAND_OF_SACRIFICE);
                }
            }

            void Register() OVERRIDE
            {
                OnEffectSplit += AuraEffectSplitFn(spell_pal_hand_of_sacrifice_AuraScript::Split, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_hand_of_sacrifice_AuraScript();
        }
};

// 20473 - Holy Shock
class spell_pal_holy_shock : public SpellScriptLoader
{
    public:
        spell_pal_holy_shock() : SpellScriptLoader("spell_pal_holy_shock") { }

        class spell_pal_holy_shock_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_holy_shock_SpellScript);

            bool Validate(SpellInfo const* spellInfo) OVERRIDE
            {
                SpellInfo const* firstRankSpellInfo = sSpellMgr->GetSpellInfo(SPELL_PALADIN_HOLY_SHOCK_R1);
                if (!firstRankSpellInfo)
                    return false;

                // can't use other spell than holy shock due to spell_ranks dependency
                if (!spellInfo->IsRankOf(firstRankSpellInfo))
                    return false;

                uint8 rank = spellInfo->GetRank();
                if (!sSpellMgr->GetSpellWithRank(SPELL_PALADIN_HOLY_SHOCK_R1_DAMAGE, rank, true) || !sSpellMgr->GetSpellWithRank(SPELL_PALADIN_HOLY_SHOCK_R1_HEALING, rank, true))
                    return false;

                return true;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                if (Unit* unitTarget = GetHitUnit())
                {
                    uint8 rank = GetSpellInfo()->GetRank();
                    if (caster->IsFriendlyTo(unitTarget))
                        caster->CastSpell(unitTarget, sSpellMgr->GetSpellWithRank(SPELL_PALADIN_HOLY_SHOCK_R1_HEALING, rank), true);
                    else
                        caster->CastSpell(unitTarget, sSpellMgr->GetSpellWithRank(SPELL_PALADIN_HOLY_SHOCK_R1_DAMAGE, rank), true);
                }
            }

            SpellCastResult CheckCast()
            {
                Unit* caster = GetCaster();
                if (Unit* target = GetExplTargetUnit())
                {
                    if (!caster->IsFriendlyTo(target))
                    {
                        if (!caster->IsValidAttackTarget(target))
                            return SPELL_FAILED_BAD_TARGETS;

                        if (!caster->isInFront(target))
                            return SPELL_FAILED_UNIT_NOT_INFRONT;
                    }
                }
                else
                    return SPELL_FAILED_BAD_TARGETS;
                return SPELL_CAST_OK;
            }

            void Register() OVERRIDE
            {
                OnCheckCast += SpellCheckCastFn(spell_pal_holy_shock_SpellScript::CheckCast);
                OnEffectHitTarget += SpellEffectFn(spell_pal_holy_shock_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_holy_shock_SpellScript();
        }
};

// 63510 - Improved Concentraction Aura (Area Aura)
// 63514 - Improved Devotion Aura (Area Aura)
// 63531 - Sanctified Retribution (Area Aura)
class spell_pal_improved_aura_effect : public SpellScriptLoader
{
    public:
        spell_pal_improved_aura_effect(char const* name) : SpellScriptLoader(name) { }

        class spell_pal_improved_aura_effect_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_improved_aura_effect_AuraScript);

            bool CheckAreaTarget(Unit* target)
            {
                Unit::AuraApplicationMap& appliedAuras = target->GetAppliedAuras();
                for (Unit::AuraApplicationMap::iterator itr = appliedAuras.begin(); itr != appliedAuras.end(); ++itr)
                {
                    Aura const* aura = itr->second->GetBase();
                    if (aura->GetSpellInfo()->GetSpellSpecific() == SPELL_SPECIFIC_AURA && aura->GetCasterGUID() == GetCasterGUID())
                    {
                        // Not allow for Retribution Aura (prevent stacking) - Retribution Aura Overflow and Retribution Aura has same spell effects
                        if (GetSpellInfo()->Id == SPELL_PALADIN_SANCTIFIED_RETRIBUTION_AURA && aura->GetSpellInfo()->SpellIconID == PALADIN_ICON_ID_RETRIBUTION_AURA)
                            return false;
                        return true;
                    }
                }

                return false;
            }

            void Register() OVERRIDE
            {
                DoCheckAreaTarget += AuraCheckAreaTargetFn(spell_pal_improved_aura_effect_AuraScript::CheckAreaTarget);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_improved_aura_effect_AuraScript();
        }
};

// 37705 - Healing Discount
class spell_pal_item_healing_discount : public SpellScriptLoader
{
    public:
        spell_pal_item_healing_discount() : SpellScriptLoader("spell_pal_item_healing_discount") { }

        class spell_pal_item_healing_discount_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_item_healing_discount_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_ITEM_HEALING_TRANCE))
                    return false;
                return true;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(GetTarget(), SPELL_PALADIN_ITEM_HEALING_TRANCE, true, NULL, aurEff);
            }

            void Register() OVERRIDE
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_item_healing_discount_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_item_healing_discount_AuraScript();
        }
};
 
// 633 - Lay on Hands
class spell_pal_lay_on_hands : public SpellScriptLoader
{
    public:
        spell_pal_lay_on_hands() : SpellScriptLoader("spell_pal_lay_on_hands") { }

        class spell_pal_lay_on_hands_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_lay_on_hands_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_FORBEARANCE) ||
                    !sSpellMgr->GetSpellInfo(SPELL_PALADIN_IMMUNE_SHIELD_MARKER))
                    return false;
                return true;
            }

            SpellCastResult CheckCast()
            {
                Unit* caster = GetCaster();
                if (Unit* target = GetExplTargetUnit())
                    if (caster == target)
                        if (target->HasAura(SPELL_PALADIN_FORBEARANCE) ||
                            target->HasAura(SPELL_PALADIN_IMMUNE_SHIELD_MARKER))
                        {
                            return SPELL_FAILED_TARGET_AURASTATE;
                        }

                return SPELL_CAST_OK;
            }

            void HandleScript()
            {
                Unit* caster = GetCaster();
                if (caster == GetHitUnit())
                {
                    caster->CastSpell(caster, SPELL_PALADIN_FORBEARANCE, true);
                    caster->CastSpell(caster, SPELL_PALADIN_IMMUNE_SHIELD_MARKER, true);
                }
            }

            void Register() OVERRIDE
            {
                OnCheckCast += SpellCheckCastFn(spell_pal_lay_on_hands_SpellScript::CheckCast);
                AfterHit += SpellHitFn(spell_pal_lay_on_hands_SpellScript::HandleScript);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_lay_on_hands_SpellScript();
        }
};

// 31789 - Righteous Defense
class spell_pal_righteous_defense : public SpellScriptLoader
{
    public:
        spell_pal_righteous_defense() : SpellScriptLoader("spell_pal_righteous_defense") { }

        class spell_pal_righteous_defense_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_righteous_defense_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_RIGHTEOUS_DEFENSE_TAUNT))
                    return false;
                return true;
            }

            SpellCastResult CheckCast()
            {
                Unit* caster = GetCaster();
                if (caster->GetTypeId() != TYPEID_PLAYER)
                    return SPELL_FAILED_DONT_REPORT;

                if (Unit* target = GetExplTargetUnit())
                {
                    if (!target->IsFriendlyTo(caster) || target->getAttackers().empty())
                        return SPELL_FAILED_BAD_TARGETS;
                }
                else
                    return SPELL_FAILED_BAD_TARGETS;

                return SPELL_CAST_OK;
            }

            void HandleTriggerSpellLaunch(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);
            }

            void HandleTriggerSpellHit(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);
                if (Unit* target = GetHitUnit())
                    GetCaster()->CastSpell(target, SPELL_PALADIN_RIGHTEOUS_DEFENSE_TAUNT, true);
            }

            void Register() OVERRIDE
            {
                OnCheckCast += SpellCheckCastFn(spell_pal_righteous_defense_SpellScript::CheckCast);
                //! WORKAROUND
                //! target select will be executed in hitphase of effect 0
                //! so we must handle trigger spell also in hit phase (default execution in launch phase)
                //! see issue #3718
                OnEffectLaunchTarget += SpellEffectFn(spell_pal_righteous_defense_SpellScript::HandleTriggerSpellLaunch, EFFECT_1, SPELL_EFFECT_TRIGGER_SPELL);
                OnEffectHitTarget += SpellEffectFn(spell_pal_righteous_defense_SpellScript::HandleTriggerSpellHit, EFFECT_1, SPELL_EFFECT_TRIGGER_SPELL);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_righteous_defense_SpellScript();
        }
};

// 85256 - Templar's Verdict
/// Updated 4.3.4
class spell_pal_templar_s_verdict : public SpellScriptLoader
{
    public:
        spell_pal_templar_s_verdict() : SpellScriptLoader("spell_pal_templar_s_verdict") { }

        class spell_pal_templar_s_verdict_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_templar_s_verdict_SpellScript);

            bool Validate (SpellInfo const* /*spellEntry*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_DIVINE_PURPOSE_PROC))
                    return false;

                return true;
            }

            bool Load()
            {
                if (!GetCaster() || GetCaster()->GetTypeId() != TYPEID_PLAYER)
                    return false;

                if (GetCaster()->ToPlayer()->getClass() != CLASS_PALADIN)
                    return false;

                return true;
            }

            void HandleBeforeCast()
            {
                if(Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_PALADIN_DIVINE_PURPOSE_PROC))
                    {
                        restoreHolyPower = true;
                        realHolyPower = caster->GetPower(POWER_HOLY_POWER);
                        caster->SetWordOfGloryHolyPower(3);
                    }
                    else
                    {
                        restoreHolyPower = false;
                    }
                }
            }
            
            void HandleAfterCast()
            {
                if(Unit* caster = GetCaster())
                {
                    // Restore Holy Power from Divine Purpose
                    if(restoreHolyPower && realHolyPower)
                    {
                        caster->SetPower(POWER_HOLY_POWER, realHolyPower);
                    }
                }
            }
            
            void ChangeDamage(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();

                if(!caster)
                    return;
                
                if (caster->HasAura(SPELL_PALADIN_DIVINE_PURPOSE_PROC))
                {
                    caster->RemoveAurasDueToSpell(SPELL_PALADIN_DIVINE_PURPOSE_PROC);
                }
            }
            
        private:
            bool restoreHolyPower;
            int8 realHolyPower;

            void Register()
            {
                BeforeCast += SpellCastFn(spell_pal_templar_s_verdict_SpellScript::HandleBeforeCast);
                AfterCast += SpellCastFn(spell_pal_templar_s_verdict_SpellScript::HandleAfterCast);
                OnEffectHitTarget += SpellEffectFn(spell_pal_templar_s_verdict_SpellScript::ChangeDamage, EFFECT_0, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pal_templar_s_verdict_SpellScript();
        }
};

// 85673 Word of Glory
class spell_paladin_word_of_glory : public SpellScriptLoader
{
    public:
        spell_paladin_word_of_glory() : SpellScriptLoader("spell_paladin_word_of_glory") { }

        class spell_paladin_word_of_glory_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_paladin_word_of_glory_SpellScript);
            
            void HandleBeforeCast()
            {
                if(Unit* caster = GetCaster())
                {
                    if (caster->HasAura(SPELL_PALADIN_DIVINE_PURPOSE_PROC))
                    {
                        restoreHolyPower = true;
                        realHolyPower = caster->GetPower(POWER_HOLY_POWER);
                        caster->SetWordOfGloryHolyPower(3);
                    }
                    else
                    {
                        restoreHolyPower = false;
                        caster->SetWordOfGloryHolyPower(caster->GetPower(POWER_HOLY_POWER));
                    }
                }
            }

            void HandleAfterCast()
            {
                if(Unit* caster = GetCaster())
                {
                    // Eternal Glory
                    if(AuraEffect* aurEff = caster->GetDummyAuraEffect(SPELLFAMILY_PALADIN, 2944, EFFECT_0))
                    {
                        if(roll_chance_i(aurEff->GetAmount()))
                        {
                            int32 bp0 = caster->GetWordOfGloryHolyPower();
                            caster->SetPower(POWER_HOLY_POWER, bp0);
                        }
                    }

                    // Restore Holy Power from Divine Purpose
                    if(restoreHolyPower && realHolyPower)
                    {
                        caster->SetPower(POWER_HOLY_POWER, realHolyPower);
                    }
                }
            }
            
            void HandleOnEffectHit(SpellEffIndex /*effIndex*/) 
            {
                if(Unit* caster = GetCaster())
                {
                    // Guarded by the Light rank 2 check
                    if(caster->HasAura(SPELL_PALADIN_GUARDED_BY_THE_LIGHT_RANK_2))
                    {
                        uint32 currHealth = caster->GetHealth();
                        uint32 maxHealth = caster->GetMaxHealth();
                        int32 overHeal = (currHealth + GetHitHeal()) - maxHealth;

                        if(overHeal > 0)
                        {
                            // Overheal shield
                            caster->CastCustomSpell(caster, SPELL_PALADIN_GUARDED_BY_THE_LIGHT_SHIELD, &overHeal, NULL, NULL, true);
                        }
                    }
                }
            }

        private:
            bool restoreHolyPower;
            int8 realHolyPower;

            void Register()
            {
                BeforeCast += SpellCastFn(spell_paladin_word_of_glory_SpellScript::HandleBeforeCast);
                AfterCast += SpellCastFn(spell_paladin_word_of_glory_SpellScript::HandleAfterCast);
                OnEffectHitTarget += SpellEffectFn(spell_paladin_word_of_glory_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_paladin_word_of_glory_SpellScript();
        }
};

// 20154, 21084 - Seal of Righteousness - melee proc dummy (addition ${$MWS*(0.022*$AP+0.044*$SPH)} damage)
class spell_pal_seal_of_righteousness : public SpellScriptLoader
{
    public:
        spell_pal_seal_of_righteousness() : SpellScriptLoader("spell_pal_seal_of_righteousness") { }

        class spell_pal_seal_of_righteousness_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_seal_of_righteousness_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_SEAL_OF_RIGHTEOUSNESS))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                return eventInfo.GetProcTarget();
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                float ap = GetTarget()->GetTotalAttackPowerValue(BASE_ATTACK);
                int32 holy = GetTarget()->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_HOLY);
                holy += eventInfo.GetProcTarget()->SpellBaseDamageBonusTaken(SPELL_SCHOOL_MASK_HOLY);
                int32 bp = int32((ap * 0.022f + 0.044f * holy) * GetTarget()->GetAttackTime(BASE_ATTACK) / 1000);
                GetTarget()->CastCustomSpell(SPELL_PALADIN_SEAL_OF_RIGHTEOUSNESS, SPELLVALUE_BASE_POINT0, bp, eventInfo.GetProcTarget(), true, NULL, aurEff);
            }

            void Register() OVERRIDE
            {
                DoCheckProc += AuraCheckProcFn(spell_pal_seal_of_righteousness_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pal_seal_of_righteousness_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_seal_of_righteousness_AuraScript();
        }
};

class spell_pal_guardian_ancient_kings : public SpellScriptLoader
{
    public:
        spell_pal_guardian_ancient_kings() : SpellScriptLoader("spell_pal_guardian_ancient_kings") { }

        class spell_pal_guardian_ancient_kings_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_guardian_ancient_kings_SpellScript);

            bool Validate (SpellInfo const* /*spellEntry*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_GUARDIAN_ANCIENT_KINGS) || !sSpellMgr->GetSpellInfo(SPELL_PALADIN_GUARDIAN_HOLY) ||
                    !sSpellMgr->GetSpellInfo(SPELL_PALADIN_GUARDIAN_RETRIBUTION) || !sSpellMgr->GetSpellInfo(SPELL_PALADIN_GUARDIAN_PROTECTION))
                    return false;

                return true;
            }

            bool Load() OVERRIDE
            {

               if (GetCaster()->GetTypeId() != TYPEID_PLAYER) //Check if caster is a player. 
                return false;

               else if(GetCaster()->ToPlayer()->getClass() != CLASS_PALADIN)
                   return false;

               else
               return true;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                {
                    //Check first spell in every spec to decide which guardian should be summoned.
					//Añadido BoT: si ya tienes un guardián es imposible sacar otro, aunque reinicies cooldown con los duelos...
                    if (caster->ToPlayer()->HasSpell(20473) && !caster->ToPlayer()->HasSpell(SPELL_PALADIN_GUARDIAN_HOLY)) 
                    {
                        caster->CastSpell(caster, SPELL_PALADIN_GUARDIAN_HOLY, false);
                        return;
                    }

                    else if (caster->ToPlayer()->HasSpell(31935) && !caster->ToPlayer()->HasSpell(SPELL_PALADIN_GUARDIAN_PROTECTION)) 
                    {
                        caster->CastSpell(caster, SPELL_PALADIN_GUARDIAN_PROTECTION, false);
                        return;
                    }

                    else if (caster->ToPlayer()->HasSpell(85256) && !caster->ToPlayer()->HasSpell(SPELL_PALADIN_GUARDIAN_RETRIBUTION)) 
                    {
                        caster->CastSpell(caster, SPELL_PALADIN_GUARDIAN_RETRIBUTION, false);
                        return;
                    }
                }
            }

            void Register() OVERRIDE
            {
                OnEffectLaunch += SpellEffectFn(spell_pal_guardian_ancient_kings_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_guardian_ancient_kings_SpellScript();
        }
};

// 20425 - Judgement of Command BoT WoW

class spell_pal_judgement_of_command : public SpellScriptLoader
{
    public:
        spell_pal_judgement_of_command() : SpellScriptLoader("spell_pal_judgement_of_command") { }

        class spell_pal_judgement_of_command_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_judgement_of_command_SpellScript);

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* unitTarget = GetHitUnit())
                    if (SpellInfo const* spell_proto = sSpellMgr->GetSpellInfo(GetEffectValue()))
                        GetCaster()->CastSpell(unitTarget, spell_proto, true, NULL);
            }

            void Register()
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_judgement_of_command_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pal_judgement_of_command_SpellScript();
        }
};

// Light of Dawn BoT WoW 
class spell_pal_lod : public SpellScriptLoader
{
    public:
        spell_pal_lod() : SpellScriptLoader("spell_pal_lod") { }

        class spell_pal_lod_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_lod_SpellScript);

            void FilterTargets(std::list<WorldObject*>& targets)
            {
                std::list<Unit*> temp;
                for (std::list<WorldObject*>::iterator itr = targets.begin(); itr != targets.end(); itr++)
                    if (Unit* unit = (*itr)->ToUnit())
                        temp.push_back(unit);

                targets.clear();
                temp.sort(Trinity::HealthPctOrderPred());
                if (temp.size() > 6)
                    temp.resize(6);                
                for (std::list<Unit*>::iterator itr = temp.begin(); itr != temp.end(); itr++)
                    targets.push_back((WorldObject*)(*itr));
            }

            void HandleOnEffectHit(SpellEffIndex /*effIndex*/)
            {
                Player * p = GetCaster()->ToPlayer();
                int32 heal = int32(GetEffectValue() + 0.156*p->SpellBaseHealingBonusDone(SPELL_SCHOOL_MASK_ALL));
                int8 hp_charges = p->GetPower(POWER_HOLY_POWER); //Formula: 606 a 874 por cada carga de Holy Power
                int32 total_heal = heal * (hp_charges + 1); //El mas 1 es porque Trinity detecta mal las cargas de poder sagrado, cuando tienes 3 te detecta solo 2, cuando tienes 2 te detecta 1 y cuando tienes 1 no te detecta ninguna.
                SetHitHeal(total_heal);
            }

            void HandleAfterCast()
            {
                Player* p = GetCaster()->ToPlayer();
                p->SetPower(POWER_HOLY_POWER, 0);           
            }

            void Register()
            {

                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pal_lod_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_CONE_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pal_lod_SpellScript::HandleOnEffectHit, EFFECT_0, SPELL_EFFECT_HEAL);
                AfterCast += SpellCastFn(spell_pal_lod_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pal_lod_SpellScript();
        }
};

// 53600 - Shield of the Righteous BoT WoW
/// Updated 4.3.4
class spell_pal_shield_of_the_righteous : public SpellScriptLoader
{
    public:
        spell_pal_shield_of_the_righteous() : SpellScriptLoader("spell_pal_shield_of_the_righteous") { }

        class spell_pal_shield_of_the_righteous_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_shield_of_the_righteous_SpellScript);

            bool Load()
            {
                if (GetCaster()->GetTypeId() != TYPEID_PLAYER)
                    return false;

                if (GetCaster()->ToPlayer()->getClass() != CLASS_PALADIN)
                    return false;

                return true;
            }

            void ChangeDamage(SpellEffIndex /*effIndex*/)
            {
                int32 damage = GetHitDamage();

                // Because 1 Holy Power (HP) is consumed when casting spell,
                // GetPower(POWER_HOLY_POWER) will return 0 when player has 1 HP,
                // return 1 at 2 HP, and 2 at 3 HP
                int32 hp = GetCaster()->GetPower(POWER_HOLY_POWER);

                // Holy Power Scaling: 3 times damage at 2 HP, 6 times at 3 HP
                damage *= 0.5*hp*hp + 1.5*hp + 1;

                SetHitDamage(damage);
            }

            void Register()
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_shield_of_the_righteous_SpellScript::ChangeDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pal_shield_of_the_righteous_SpellScript();
        }
};


//  20165 Seal of Insight
//DELETE FROM spell_script_names WHERE spell_id = 20165;
//INSERT INTO spell_script_names VALUES (20165,'spell_pal_seal_of_insight');

class spell_pal_seal_of_insight : public SpellScriptLoader
{
    public:
        spell_pal_seal_of_insight() : SpellScriptLoader("spell_pal_seal_of_insight") { }

        class spell_pal_seal_of_insight_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_seal_of_insight_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_SEAL_OF_INSIGHT))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();
                if(Player* p = GetCaster()->ToPlayer())
                {
                    int32 heal_amount = int32(0.15*p->GetTotalAttackPowerValue(BASE_ATTACK)+0.15*p->SpellBaseHealingBonusDone(SPELL_SCHOOL_MASK_ALL));
                    int32 mana_amount = p->GetMaxPower(POWER_MANA) * 0.04;
                    p->CastCustomSpell(p,SPELL_PALADIN_SEAL_OF_INSIGHT,&heal_amount,&mana_amount,NULL,true);
                }

            }

            void Register() OVERRIDE
            {
                OnEffectProc += AuraEffectProcFn(spell_pal_seal_of_insight_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const OVERRIDE
        {
            return new spell_pal_seal_of_insight_AuraScript();
        }
};

// Inquisition BoT WoW
class spell_pal_inquisition : public SpellScriptLoader
{
    public:
        spell_pal_inquisition() : SpellScriptLoader("spell_pal_inquisition") { }

        class spell_pal_inquisition_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_inquisition_SpellScript);

            void HandleApply(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                int8 holypower = caster->GetPower(POWER_HOLY_POWER);
          int32 total_power = (holypower + 1);
                if (caster->HasAura(SPELL_PALADIN_DIVINE_PURPOSE_PROC))
                holypower = GetCaster()->GetMaxPower(POWER_HOLY_POWER);
                Aura* aura = GetHitAura();
                aura->SetDuration(aura->GetMaxDuration() * total_power);
            }
            
         void HandleAfterCast()
            {
                Player* p = GetCaster()->ToPlayer();
                p->SetPower(POWER_HOLY_POWER, 0);           
         }
            
            void Register()
            {
                OnEffectHitTarget += SpellEffectFn(spell_pal_inquisition_SpellScript::HandleApply, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
          AfterCast += SpellCastFn(spell_pal_inquisition_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pal_inquisition_SpellScript();
        }
};

// MAESTRIA PROTECCION baluarte divino 
// (76671, 'spell_pal_divine_bulwark')

class spell_pal_divine_bulwark : public SpellScriptLoader
{
    public:
        spell_pal_divine_bulwark() : SpellScriptLoader("spell_pal_divine_bulwark") { }

        class spell_pal_divine_bulwark_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_divine_bulwark_AuraScript);

            void CalculateBonus(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = true;

                if (!GetCaster())
                    return;

                // Divine Bulwark (Pally Prote mastery)
                if (Player* caster = GetCaster()->ToPlayer())
                    if (caster->HasAuraType(SPELL_AURA_MASTERY))
                        if (caster->GetPrimaryTalentTree(caster->GetActiveSpec()) == BS_PALADIN_PROTECTION)
                            amount += int32(2.25f * caster->GetMasteryPoints());
                            
            }

            void Register()
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_divine_bulwark_AuraScript::CalculateBonus, EFFECT_0, SPELL_AURA_MOD_BLOCK_PERCENT);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pal_divine_bulwark_AuraScript();
        }
};

// 85117, 86172 - PROPOSITO DIVINO BoT WoW
class spell_pal_divine_purpose : public SpellScriptLoader
{
    public:
        spell_pal_divine_purpose() : SpellScriptLoader("spell_pal_divine_purpose") { }

        class spell_pal_divine_purpose_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_divine_purpose_SpellScript)

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALA_DIVINEPOR_R1) || !sSpellMgr->GetSpellInfo(SPELL_PALA_DIVINEPOR_R2))
                    return false;
                return true;

            }

			bool Load() OVERRIDE
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleOnHit()
            {
				Player* caster = GetCaster()->ToPlayer(); 
				if (GetCaster()->HasAura(SPELL_PALA_DIVINEPOR_R1)) // rango 1 proposito divino
					{
						if (roll_chance_i(7.0f)) // 7% de prob de lanzarlo
						{
							GetCaster()->CastSpell(caster, SPELL_PALADIN_DIVINE_PURPOSE_PROC, true);
						}
					}
				else if (GetCaster()->HasAura(SPELL_PALA_DIVINEPOR_R2)) //rango 2 proposito divino
					{
						if (roll_chance_i(15.0f)) // 15% de prob de lanzarlo
						{
							GetCaster()->CastSpell(caster, SPELL_PALADIN_DIVINE_PURPOSE_PROC, true);
						}
					}
            }

            void Register() OVERRIDE
            {
                OnHit += SpellHitFn(spell_pal_divine_purpose_SpellScript::HandleOnHit);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_divine_purpose_SpellScript();
        }
};

// COLERA SANTIFICADA
class spell_pal_cs : public SpellScriptLoader
{
    public:
        spell_pal_cs() : SpellScriptLoader("spell_pal_cs") { }

        class spell_pal_cs_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_cs_SpellScript)

            bool Validate(SpellInfo const* /*spellInfo*/) OVERRIDE
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PALADIN_CS_R1) || !sSpellMgr->GetSpellInfo(SPELL_PALADIN_CS_R2) || !sSpellMgr->GetSpellInfo(SPELL_PALADIN_CS_R3))
                    return false;
                return true;

            }

            bool Load() OVERRIDE
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleOnHit()
            {
				Player* caster = GetCaster()->ToPlayer(); 
				if (GetCaster()->HasAura(SPELL_PALADIN_CS_R1)) // rango 1 colera santificada
					{
						GetCaster()->CastSpell(caster,SPELL_PALADIN_CS_AURA, true); // 100% de prob de lanzarlo
					}

				else if (GetCaster()->HasAura(SPELL_PALADIN_CS_R2)) //rango 2 colera santificada
					{
						GetCaster()->CastSpell(caster, SPELL_PALADIN_CS_AURA, true); //100% de prob de lanzarlo
					}

				else if (GetCaster()->HasAura(SPELL_PALADIN_CS_R3)) //rango 3 colera santificada
					{
						GetCaster()->CastSpell(caster, SPELL_PALADIN_CS_AURA, true); //100% de prob de lanzarlo
					}
            }

            void Register() OVERRIDE
            {
                OnHit += SpellHitFn(spell_pal_cs_SpellScript::HandleOnHit);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_cs_SpellScript();
        }
};

// vida vendita talento Sagrado (heal)
// 89023 - Blessed Life
class spell_pal_blessed_life : public SpellScriptLoader
{
public:
    spell_pal_blessed_life() : SpellScriptLoader("spell_pal_blessed_life") { }

    class spell_pal_blessed_life_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_pal_blessed_life_SpellScript);

        void HandleAfterCast()
        {
            Unit * caster = GetCaster();

            if (!caster || !caster->ToPlayer())
                return;
            
            caster->ToPlayer()->AddSpellCooldown(GetSpellInfo()->Id, 0, time(NULL) + 8);
        }

        void Register() OVERRIDE
        {
            AfterCast += SpellCastFn(spell_pal_blessed_life_SpellScript::HandleAfterCast);
        }
    };

    SpellScript *GetSpellScript() const OVERRIDE
    {
        return new spell_pal_blessed_life_SpellScript();
    }
};

// limpiar spell general 
// 4987 - Cleanse
class spell_pal_cleanse : public SpellScriptLoader
{
    public:
        spell_pal_cleanse() : SpellScriptLoader("spell_pal_cleanse") { }

        class spell_pal_cleanse_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pal_cleanse_SpellScript);
            
            void HandleOnHit()
            {
                Unit* target = GetHitUnit();
                Unit* caster = GetCaster();

                if(!caster || !target)
                    return;
                    
                if(caster == target)
                {
                    // Acts of Sacrifice
                    if(caster->HasAura(85446) || caster->HasAura(85795))
                    {
                        caster->RemoveAurasWithMechanic((1<<MECHANIC_SNARE)|(1<<MECHANIC_ROOT));
                    }
                }
            }

            void Register() OVERRIDE
            {
                OnHit += SpellHitFn(spell_pal_cleanse_SpellScript::HandleOnHit);
            }
        };

        SpellScript* GetSpellScript() const OVERRIDE
        {
            return new spell_pal_cleanse_SpellScript();
        }
};

// comunion
// 465, 19746, 19891 Communion ammount with auras
class spell_pal_communion : public SpellScriptLoader
{
    public:
        spell_pal_communion() : SpellScriptLoader("spell_pal_communion") { }

        class spell_pal_communion_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pal_communion_AuraScript);

            void ChangeAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;

                if (Unit* caster = GetCaster())
                {
                    if(caster->HasAura(31876))
                        amount = 3;
                }       
            }

            void Register()
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pal_communion_AuraScript::ChangeAmount, EFFECT_1, SPELL_AURA_MOD_DAMAGE_PERCENT_DONE);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pal_communion_AuraScript();
        }
};

// 498 - Divine Protection
class spell_pal_divine_protection : public SpellScriptLoader
{
public:
    spell_pal_divine_protection() : SpellScriptLoader("spell_pal_divine_protection") { }

    class spell_pal_divine_protection_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_pal_divine_protection_SpellScript);

        void HandleOnCast()
        {
            Unit * caster = GetCaster();

            if (!caster)
                return;
            
            // Speed of Light
            if (caster->GetAuraEffect(SPELL_AURA_DUMMY, SPELLFAMILY_PALADIN, 5062, EFFECT_1))
                caster->CastSpell(caster, SPELL_PALADIN_SPEED_OF_LIGHT, false);
        }

        void Register()
        {
            OnCast += SpellCastFn(spell_pal_divine_protection_SpellScript::HandleOnCast);
        }
    };

    SpellScript *GetSpellScript() const
    {
        return new spell_pal_divine_protection_SpellScript();
    }
};

void AddSC_paladin_spell_scripts()
{
    new spell_pal_ardent_defender();
    new spell_pal_aura_mastery();
    new spell_pal_aura_mastery_immune();	
    new spell_pal_lights_beacon();
    new spell_pal_blessing_of_faith();
    new spell_pal_divine_sacrifice();
    new spell_pal_divine_storm();
    new spell_pal_divine_storm_dummy();
    new spell_pal_exorcism_and_holy_wrath_damage();
    new spell_pal_eye_for_an_eye();
    new spell_pal_grand_crusader();
    new spell_pal_hand_of_sacrifice();
    new spell_pal_holy_shock();
    new spell_pal_improved_aura_effect("spell_pal_improved_concentraction_aura_effect");
    new spell_pal_improved_aura_effect("spell_pal_improved_devotion_aura_effect");
    new spell_pal_improved_aura_effect("spell_pal_sanctified_retribution_effect");
    new spell_pal_item_healing_discount();
    new spell_pal_lay_on_hands();
    new spell_pal_righteous_defense();
    new spell_pal_seal_of_righteousness();
    new spell_pal_guardian_ancient_kings();
    new spell_pal_judgement_of_command();
    new spell_pal_shield_of_the_righteous();
    new spell_pal_seal_of_insight();
    new spell_pal_lod();
    new spell_pal_inquisition();
    new spell_pal_divine_bulwark();
	new spell_pal_divine_purpose();
	new spell_pal_cs();
	new spell_pal_blessed_life();
	new spell_pal_cleanse();
	new spell_pal_communion();
	new spell_pal_divine_protection();
    new spell_pal_templar_s_verdict();	
    
}