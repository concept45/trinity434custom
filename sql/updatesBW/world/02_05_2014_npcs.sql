-- http://es.wowhead.com/npc=46493 quitaba 13 de vida
UPDATE creature_template SET mindmg=1012.30769, maxdmg=1300 WHERE entry=46493

-- http://www.wowhead.com/npc=50245
UPDATE creature_template SET mindmg=4000, maxdmg=5000 WHERE entry=50245;

-- http://es.wowhead.com/npc=46624
UPDATE creature_template SET unit_flags=0, npcflag=0 WHERE entry=46624;

-- http://es.wowhead.com/quest=27647
UPDATE gameobject_template SET data8=27647 WHERE entry=206203;
INSERT INTO `world`.`gameobject_loot_template`(`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`)VALUES('206203','62317','100','1','0','1','1');

-- http://es.wowhead.com/npc=46145
UPDATE creature_template SET health_mod=1.5001443 WHERE entry=46145;

-- http://es.wowhead.com/npc=36915
UPDATE creature_template_addon SET auras=NULL WHERE entry=36915;

-- http://es.wowhead.com/npc=48704
UPDATE creature_template_addon SET auras=NULL WHERE entry=48704;

-- http://es.wowhead.com/npc=46654
UPDATE creature_template_addon SET auras=NULL WHERE entry=46654;

-- http://es.wowhead.com/npc=47720
UPDATE creature_template SET unit_flags=0 WHERE entry=47720;

-- quest http://es.wowhead.com/quest=12794
UPDATE creature_template SET AIName='SmartAI' WHERE entry=23729;
UPDATE creature_template SET AIName='SmartAI' WHERE entry=26673;
UPDATE creature_template SET AIName='SmartAI' WHERE entry=27158;
UPDATE creature_template SET AIName='SmartAI' WHERE entry=29158;
UPDATE creature_template SET AIName='SmartAI' WHERE entry=29161;
