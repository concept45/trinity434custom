## CADENA DRAGONES ABISALES

-- http://es.wowhead.com/quest=10804 ( al llamar a los dragones para que se alimenten estos no llegan)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 10804;

-- http://es.wowhead.com/quest=10814 ( el NPC no da la opcion de hablar y no hay nada que cambiar en las tablas para que se pueda autocompletar la QUEST excepto el "Method" )
UPDATE `quest_template` SET `Method`= 0 WHERE `Id`= 10814;

-- http://es.wowhead.com/quest=10854 ( el cristal no funciona como debe sobre los objetivos y no cuenta el uso para la quest )
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 10854;

-- Falta el Intendente en Shattrath que vende los Dragones

INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
( 3000101, 23489, 530, 1, 1, 0, 0, -1615.709961, 5265.000000, -40.762501, 3.277240, 300, 0, 0, 6986, 0, 0, 0, 0, 0 );
UPDATE `creature_template` SET `faction_A`= 35, `faction_H`= 35 WHERE `entry`=23489;



## Teldrassil

-- http://es.wowhead.com/quest=2541 (al matar a los chamanes no suelta el item de la mision)
DELETE FROM creature_loot_template WHERE entry = 2009 AND item = 8363;
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(2009, 8363, -23, 1, 0, 1, 1);




## Costa Oscura (NPC duplicados por toda la Zona)

-- http://es.wowhead.com/quest=13882 (los NPC no dan los items necesarios para completar la mision)
UPDATE `quest_template` SET `RequiredItemCount1`= 0, `RequiredItemCount2`= 0, `RequiredItemCount3`= 0 WHERE `Id`=13882;


-- http://es.wowhead.com/quest=13544 ( Pataveloz aparece como amistodo y no dropea el item de la mision )  DBGUID = 81296
UPDATE `creature_template` SET `faction_A`= 7, `faction_H`= 7 WHERE `entry`=32997;
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(32997, 44886, -100, 1, 0, 1, 1);


-- http://es.wowhead.com/quest=13545 (los objetivos aparecen como amistosos)
UPDATE `creature_template` SET `faction_A`= 7 WHERE `entry`=33000;


-- http://es.wowhead.com/quest=13925 ( al usar el item de la mision esta no se completa)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 13925;


-- http://es.wowhead.com/quest=13885
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`=13885;


-- http://es.wowhead.com/quest=13891 (falta el npc y el item no funciona en la locazion, pero con matar al NPC se compelta la mision)
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(16424987, 34331, 1, 1, 1, 0, 0, 4796.307129, 79.696617, 48.120361, 0.942720, 120, 0, 0, 866, 0, 0, 0, 0,0 );


-- http://es.wowhead.com/quest=13918 ( el item no funciona para buscar los contenedores )
UPDATE `quest_template` SET `RequiredItemCount1`= 0, `RequiredItemCount2`= 0 WHERE `Id`= 13918;


-- http://es.wowhead.com/quest=13907 (no muestra a los objetivos para asesinar)
UPDATE `quest_template` SET `RequiredNpcOrGo1`= 2207 WHERE `Id`= 13907;


-- http://es.wowhead.com/quest=13910 ( no se puede construir la casa)
UPDATE `quest_template`SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 13910;





## QUEST VARIAS

-- http://es.wowhead.com/quest=28114 (los NPC no dan los items necesarios para completar la quest)
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(47203, 63029, -100,  1, 0, 2, 8);
UPDATE `creature_template` SET `lootid`= 47203, `questItem1`= 63209 WHERE `entry`= 47203;

-- http://es.wowhead.com/quest=28096 (el caballo no se puede montar y los NPC de la mision no bienen )
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 28096;

-- http://es.wowhead.com/quest=28138 (no se pueden capturar los Humanos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 28138;

-- http://es.wowhead.com/quest=26420 (faltan Objetivos que dropeen los Gusanos)
INSERT INTO gameobject (guid, id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(NULL, 204281, 0, 1, 1, -8163.001465, 618.051392, 69.210793, 0.324377, 0, 0, 0, 0, 30, 100, 1 ),
(NULL, 204281, 0, 1, 1, -8163.478516, 597.297791, 72.946198, 0.191037, 0, 0, 0, 0, 30, 100, 1 );

-- http://es.wowhead.com/quest=13621 (no se puede interactuar con el NPC ni completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 13621;

-- http://es.wowhead.com/quest=11960 (no se se�alan los objetivos y no sueltan los items necesarios para completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGo1`= 26200, `RequiredNpcOrGoCount1`= 12 WHERE `Id`= 11960;

-- http://es.wowhead.com/quest=26177 (los cangrejos no sueltan los items necesarios para completar la quest)
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(42339, 57175, -100, 1, 0, 1, 1);
UPDATE `creature_template` SET `lootid`= 42339, `questItem1`= 57175 WHERE   `entry`= 42339;

-- http://es.wowhead.com/quest=27995 (pones /bailar y la mision no se completa)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`=27995;

-- http://es.wowhead.com/quest=25814 (un cuerno correctamente y se cuenta para la quest esta pero al usarlo no llama al NPC  que se debe asesinar para completar toda la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount2`= 0 WHERE `Id`=25814;

-- http://es.wowhead.com/quest=11134 (no esta el NPC , admea s la mision no se peude completar auqnue se coloque el NPC)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`=11134;

-- http://es.wowhead.com/quest=25029 (falta 1 NPC para completar la quest, los 4 NPC dan el loot solo hace falta agregar uno)
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
( NULL, 1654, 0, 1, 1, 0, 0, 2904.727295, 943.899353, 115.254303, 5.008770, 120, 0, 0, 176, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=27671 (el uso del item para teletransportar a los enanos funciona pero no cuenta los enanos teletransportados)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 27671;

-- http://es.wowhead.com/quest=12969 (el NPC sale en amistoso)
UPDATE  `creature_template` SET `Faction_A`=  7,  `Faction_H`=  7  WHERE `entry`= 30154;

-- http://es.wowhead.com/quest=12971 (los objetivos salen en amistoso pero son necesarios para otras misiones)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12971;

-- http://es.wowhead.com/quest=12856 (todo funciona pero no los objetivos no son contados al entregar la mision)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0 WHERE `Id`= 12856;

-- http://es.wowhead.com/quest=27347 (el gameobject no tira el debuff necesario para poder usar el item de la Quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 27347;

-- http://es.wowhead.com/quest=27336 (no funciona el totem de la mision, matando a los NPC referidos a la mision se puede completar la Quest)
UPDATE `quest_template` SET `RequiredNpcOrGo1`= 4345 WHERE `Id`= 27336;

-- http://es.wowhead.com/quest=27296  (no se peude quemar los objetivos con la antorcha)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`=  0, `RequiredNpcOrGoCount2`=  0, `RequiredNpcOrGoCount3`=  0 WHERE `Id`= 27296;

-- http://es.wowhead.com/quest=27410 (al usar el totem este no llama a los espiritus para matarlos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0  WHERE `Id`= 27410;

-- http://es.wowhead.com/quest=27411 (no se puede colocar el estandarte )
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0  WHERE `Id`= 27411;

-- http://es.wowhead.com/quest=25663 (falta 1 object para poder usar el item proporcionado en la mision, el NPC que aparece para completar la quest debe ser aliado para ambas facciones)
INSERT INTO gameobject (guid, id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(NULL, 203147, 1, 1, 1, 4940.899902, -2642.260010, 1428.579956, 2.842997, 0, 0, 0, 0, 30, 100, 1 );

UPDATE `creature_template` SET `faction_A`=  35, `faction_H`=  35  WHERE `entry`= 41068;

-- http://es.wowhead.com/quest=27236 (faltan los objects que dropean los items para completar la quest)
INSERT INTO gameobject (guid, id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(NULL, 186273, 1, 1, 1, -2687.432129, -4231.711426, 3.312830, 4.694046, 0, 0, 0, 0, 45, 100, 1 ),
(NULL, 186272, 1, 1, 1, -2686.728516, -4265.728516, 4.541872, 3.395405, 0, 0, 0, 0, 45, 100, 1 );

-- http://es.wowhead.com/quest=26595 (algunos objetivos tienen mal la faccion)
UPDATE `creature_template` SET  `faction_A`= 7 WHERE `entry`= 43377;
UPDATE `creature_template` SET  `faction_H`= 7 WHERE `entry`=43376;

-- http://es.wowhead.com/quest=13712 (falta el NPC para entregar la quest)
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(NULL, 33837, 1, 1, 1, 0, 0, 2279.574219, -2546.876953, 98.329468, 2.992855, 300, 0, 0, 2013, 0, 0, 0, 0, 0);

-- http://es.wowhead.com/quest=28395 (ninguno de los objetivos suelta el item para completar la quest)
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(9464, 21377, -46, 1, 0, 1, 1),
(7156, 21377, -44, 1, 0, 1, 1),
(7158, 21377, -44, 1, 0, 1, 1),
(7157, 21377, -43, 1, 0, 1, 1),
(7154, 21377, -23, 1, 0, 1, 1),
(7153, 21377, -20, 1, 0, 1, 1),
(9462, 21377, -20, 1, 0, 1, 1),
(7155, 21377, -17, 1, 0, 1, 1),
(14342, 21377, -6, 1, 0, 1, 1);

-- http://es.wowhead.com/quest=11677 (no cuenta el uso de la semilla en el caldero para completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 11677;

-- http://es.wowhead.com/quest=11684 (no se puede completar, al explorar las zonas no las cuenta para la mision)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 11684;
-- http://es.wowhead.com/quest=11706 (el cuerno no cumple con su cometido y su uso no cuenta para completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0 WHERE `Id`= 11706;
-- http://es.wowhead.com/quest=11881 (el silbato no llama el NCP que deber llevarse los  items encontrados para completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 11881;

-- http://es.wowhead.com/quest=12140 (al hablar con el tauren este no completa la mision)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12140;

-- http://es.wowhead.com/quest=24766 (el hechizo no se peude usar en los mu�ecos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 24766;

-- http://es.wowhead.com/quest=12630 (no funciona el NPC que debe acompa�arte y no puedes usar el item provisto para la quest )
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12630;

-- http://es.wowhead.com/quest=12903 (no estan los NPC para completar la quest y aun colocandolos estos no te dan la opcion para hablar con ellos y terminar la quest )
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 12903;

-- http://es.wowhead.com/quest=12740 (usas los paracaidas en los NPC pero no se cuenta su uso para la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12740;

-- http://es.wowhead.com/quest=12527 (los cocodrilos no dan el ITEM de mision )
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(28145, 38382, -100, 1, 0, 1, 1);

-- http://es.wowhead.com/quest=13549 (No cuenta el uso de la servatana en los osos ni en los leopardos )
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0 WHERE `Id`= 13549;

-- http://es.wowhead.com/quest=12622 (todo funciona bien excepto en primer objetivo ya que no esta colocado el elite ni el gameobject, colocando al elite este golpea a diestra y siniestra, la mejor opcion es colocar el gameobject)
INSERT INTO gameobject (guid, id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(NULL, 190614, 571, 1, 1, 5551.915527, -3466.465332, 350.366913,  0.745110, 0, 0, 0, 0, 30, 100, 1 );

-- http://es.wowhead.com/quest=12659 (no se puede cortar la cabeza de los objetivos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12659;

-- http://es.wowhead.com/quest=12598 (el uso de las grandas no funciona en los crateres y no las cuenta para la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12598;

-- http://es.wowhead.com/quest=12596 (no se puede hablar con los capitanes)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 12596;

-- http://es.wowhead.com/quest=12609 (los NPC no pueden ser seleccionados)
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 28221;

-- http://es.wowhead.com/quest=12504 (nose puede hablar con los soldados)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12504;

-- http://es.wowhead.com/quest=12606 (al destruir los capullos no se obtiene a ningun soldado de la cruzada)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12606;

-- http://es.wowhead.com/quest=14189 (el gameobject no termina la mision y no da la mision que sigue en la cadena)
INSERT INTO gameobject_questender (id, quest) VALUES
(195433, 14189);
INSERT INTO gameobject_queststarter (id, quest) VALUES
(195433, 14191);

-- http://es.wowhead.com/quest=14191 (el gameobject no termina la mision, ademas al pasar por las zonas esta no se marcan exploradas en el mapa, y el gameobject no inicia la siguiente mision de la cadena)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 14191;
INSERT INTO gameobject_questender (id, quest) VALUES
(195433, 14191);
INSERT INTO gameobject_queststarter (id, quest) VALUES
(195433, 14360);

-- http://es.wowhead.com/quest=14360 (el game object no termina la mision y no de la siguiente, ademas el ritual no se peude llevar a cabo)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 14360;
INSERT INTO gameobject_questender (id, quest) VALUES
(195433, 14360);
INSERT INTO gameobject_queststarter (id, quest) VALUES
(195433, 14195);

-- http://es.wowhead.com/quest=26228 (no se peude modificar nada en las tablas para hacer que se autocomplete la quest)
UPDATE `quest_template`SET `Method`= 0 WHERE `Id`= 26228;

-- http://es.wowhead.com/quest=12790 (el telport para ir a Dalaran no funciona)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0 WHERE `Id`= 12790;

-- http://es.wowhead.com/quest=12231 (sale la opcion para hablar con los 2 osos pero no se puede completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0 WHERE `Id`= 12231;

-- http://es.wowhead.com/quest=12948 (el NPC no sale a luchar y en las tablas no sale nada para poder hacerla autocompletable)
UPDATE `quest_template` SET `Method`= 0 WHERE `Id`= 12948;

-- http://es.wowhead.com/quest=27446 (el npc no puede ser visto, pero si ataca)
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 38535 ;
-- http://es.wowhead.com/quest=27447 (el npc tampoco puede ser visto pero si ataca)
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 38534;

-- http://es.wowhead.com/quest=28488 (el objetivo de la quest no tiene loot y no se puede seleccionar)
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 48639;
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(48639, 64404, -96, 1, 0, 1, 1);
UPDATE `creature_template` SET `lootid`= 48639, `questItem1`= 64404 WHERE   `entry`= 48639;
-- http://es.wowhead.com/quest=27188 (al matar a los fantasmas no aparecen los objeticos de la mision y aun colocandolos estos no te cuentan para terminar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 27188;

-- http://es.wowhead.com/quest=13654 (los esqueletos no sueltan el F�mur para incapacitar al ESCUDERO y el Escudero no suelta el item objetivo de mision)
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(33499, 45080, -32, 1, 0, 1, 1),
(33498, 45082, -100, 1, 0, 1, 1);
UPDATE `creature_template` SET `lootid`= 33499, `questItem1`= 45080 WHERE `entry`= 33499;
UPDATE `creature_template` SET `lootid`= 33498, `questItem1`= 45082 WHERE `entry`= 33498;

-- http://es.wowhead.com/quest=13663 (el hipogrifo no funciona, y aunque esten bien los items no se sabe donde esta la casa del Caballero negro " no sale en el mapa" porque el hipogrifo no les lleva al lugar)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredItemCount2`= 0, `RequiredItemCount3`= 0  WHERE `Id`= 13663;

-- http://es.wowhead.com/quest=26997 (no se puede interrogar a los ciudadanos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0  WHERE `Id`= 26997;

-- http://es.wowhead.com/quest=24993 (no estan los NPC de la mision, al a�adirlos si se puede completar la quest)
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(NULL, 38967, 0, 1, 1, 0, 0, 2232.498047, 257.691528, 33.573792, 4.020248, 45, 0, 0,  274, 0, 0, 0, 0, 0),
(NULL, 38967, 0, 1, 1, 0, 0, 2243.808594, 257.266327, 33.573711, 6.093699, 45, 0, 0,  274, 0, 0, 0, 0, 0),
(NULL, 38967, 0, 1, 1, 0, 0, 2237.678467, 275.679291, 33.402424, 0.556750, 45, 0, 0,  274, 0, 0, 0, 0, 0),
(NULL, 38967, 0, 1, 1, 0, 0, 2235.301514, 283.848328, 33.402424, 0.556750, 45, 0, 0,  274, 0, 0, 0, 0, 0),
(NULL, 38967, 0, 1, 1, 0, 0, 2219.657715, 268.467834, 33.585148, 5.108130, 45, 0, 0,  274, 0, 0, 0, 0, 0),
(NULL, 38967, 0, 1, 1, 0, 0, 2251.824951, 285.013885, 33.378819, 0.537113, 45, 0, 0,  274, 0, 0, 0, 0, 0),
(NULL, 38967, 0, 1, 1, 0, 0, 2233.325439, 266.009399, 33.461464, 3.466648, 45, 0, 0,  274, 0, 0, 0, 0, 0);


-- http://es.wowhead.com/quest=26616 ( solo podemos usar el METHOD para poder seguir con la  cadena de misiones)

UPDATE `quest_template`SET `Method`= 0 WHERE `Id`= 26616;

-- http://es.wowhead.com/quest=28203 ( falta el NPC al cual entregamos la quest)
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(NULL, 48109, 0, 1, 1, 0, 0, -7952.063477, -1912.114990, 131.703918, 0.179785, 300, 0, 0, 9812, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=9164 (no cuenta cuando rescatas a los objetivos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 9164;

-- http://es.wowhead.com/quest=28698 (los gameobjects no sueltan el objetivo de mision)
INSERT INTO gameobject_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(206586, 62818, -100, 1, 0, 1, 2);

-- http://es.wowhead.com/quest=25165 (los NPC no sueltan los  items necesarios para terminar la quest)
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(3125, 52505, -23, 1, 0, 1, 1);

-- http://es.wowhead.com/quest=25165 (no se puede recoger el veneno usando el totem)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0  WHERE `Id`= 25165;

-- http://es.wowhead.com/quest=27599 (los objetivos de la mision no dan los items)
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(45701, 61923, -100, 1, 0, 1, 1);

-- http://es.wowhead.com/quest=27845 (no se funciona el marcar a los objetivos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0  WHERE `Id`= 27845;

-- http://es.wowhead.com/quest=27600 (no existen los Gameobjects y faltan los loots)
INSERT INTO gameobject_template (entry, TYPE, displayId, NAME, IconName, castBarCaption, unk1, faction, flags, size, questItem1,  questItem2, questItem3, questItem4, questItem5, questItem6, data0, data1, data2, data3, data4, data5, data6, data7, data8,  data9, data10, data11, data12, data13, data14, data15, data16, data17, data18, data19, data20, data21, data22, data23, data24,  data25, data26, data27, data28, data29, data30, data31, unkInt32, AIName, ScriptName, WDBVerified) VALUES 
(205826, 3, 7548, 'Thousand-Thread-Count Fuse', '', '', '', 0, 0, 2, 61921, 0, 0, 0, 0, 0, 43, 34739, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 14545),
(205827, 3, 9945, 'Extra-Pure Blasting Powder', '', '', '', 0, 0, 2, 61922, 0, 0, 0, 0, 0, 43, 34740, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 14545),
(205828, 3, 43, 'Stack of Questionable Publications', '', '', '', 0, 0, 2, 61373, 0, 0, 0, 0, 0, 43, 34741, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 14545);
INSERT INTO gameobject_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(205826, 61921, -100, 1, 0, 1, 1),
(205827, 61922, -100, 1, 0, 1, 1),
(205828, 61373, -100, 1, 0, 1, 1);
INSERT INTO gameobject (guid, id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(@guid01+1, 205826, 0, 1, 1, -9967.250977, -4552.888672, 11.646503, 1.959338, 0, 0, 0, 0, 60, 100, 1 ),
(@guid01+1, 205827, 0, 1, 1, -9981.119141, -4543.564453, 6.351149, 5.548898, 0, 0, 0, 0, 60, 100, 1 ),
(@guid01+1, 205828, 0, 1, 1, -10004.051758, -4560.845215, 7.047289, 0.765562, 0, 0, 0, 0, 60, 100, 1 );

-- http://es.wowhead.com/quest=25695 (no funciona en minimapa)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 25695;

-- http://es.wowhead.com/quest=27136 (no se le peude entregar la mision y no podemos modificar nada mas en la tabla)
UPDATE `quest_template`SET `Method`= 0 WHERE `Id`= 27136;

-- http://es.wowhead.com/quest=11058 (el ritual del arcoiris no funciona XD)
UPDATE `quest_template`SET `Method`= 0 WHERE `Id`= 11058;

-- http://es.wowhead.com/quest=28495 (no funciona la varita)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 28495;

-- http://es.wowhead.com/quest=26710 (no esta el NPC y aun colocandolo no se le puede hablar)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 26710;

-- http://es.wowhead.com/quest=27048 (1 objetivo no se puede seleccionar , y ninguno de ellos tiene loot, ademas faltan algunos gameobject ya que la bomba no funciona)
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 42766;
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(47071, 65504, -22, 1, 0, 1, 1),
(49816, 65504, -10, 1, 0, 1, 1),
(42766, 65504, -13, 1, 0, 1, 1),
(44257, 65504, -15, 1, 0, 1, 1),  
(44257, 65507, -63, 1, 0, 1, 1),
(44259, 65504, -16, 1, 0, 1, 1);
UPDATE `creature_template` SET `lootid`= 47071, `questItem1`= 65504 WHERE `entry`= 47071;
UPDATE `creature_template` SET `lootid`= 44259 , `questItem1`= 65504 WHERE `entry`= 44259 ;
INSERT INTO gameobject (guid, id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(NULL, 207383, 646, 1, 1, 1959.376343, -182.647522, -176.552765, 4.827241, 0, 0, 0, 0, 3, 100, 1 ),
(NULL, 207383, 646, 1, 1, 2054.059326, -176.276184, -176.276184, 3.285174, 0, 0, 0, 0, 3, 100, 1 ),
(NULL, 207383, 646, 1, 1, 2032.055420, -202.782578, -175.445374, 1.109324, 0, 0, 0, 0, 3, 100, 1 ),
(NULL, 207384, 646, 1, 1, 2020.832031, -155.199570, -176.005157, 4.755241, 0, 0, 0, 0, 3, 100, 1 ),
(NULL, 207384, 646, 1, 1, 2015.529297, -200.108521, -175.838623, 2.364378, 0, 0, 0, 0, 3, 100, 1 ),
(NULL, 207384, 646, 1, 1, 2029.464722, -153.912201, -176.188965, 4.112819, 0, 0, 0, 0, 3, 100, 1 );

-- http://es.wowhead.com/quest=10340 (el NPC aparece muerto)
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 20234;

-- http://es.wowhead.com/quest=10895 (no cuenta el uso de la baliza)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0, `RequiredNpcOrGoCount4`= 0 WHERE `Id`= 10895;

-- http://es.wowhead.com/quest=28501 ( solo queda suar el method)
UPDATE `quest_template`SET `Method`= 0 WHERE `Id`= 28501;

-- http://es.wowhead.com/quest=27990 (no funciona el ca�on por lo cual no se puede completar la quest)
UPDATE `quest_template` SET `RequiredItemCount1`= 0, `RequiredItemCount2`= 0  WHERE `Id`=27990;

-- http://es.wowhead.com/quest=12907 (el objetivo tiene muy poco vida y se muere apenas revive)
UPDATE `creature` SET `spawntimesecs`= 10 WHERE  `guid`= 212047;
UPDATE `creature` SET `spawntimesecs`= 10 WHERE  `guid`= 216221;
UPDATE `creature` SET `spawntimesecs`= 10 WHERE  `guid`= 216220;

-- http://es.wowhead.com/quest=13425 (el vial no funciona sobre los huevos y no cuenta su uso para la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 13425;

-- http://es.wowhead.com/quest=27043 (los NPC no dan el debuff pero con matar al objetivo principal se puede completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 27043;

-- http://es.wowhead.com/quest=27932/el-hacha-de-hendimiento-terrestre (el hacha no funciona en los objetivos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 27932;

-- http://es.wowhead.com/quest=27008 (al pasar por la puerta no se autocompleta la quest)
UPDATE `quest_template`SET `Method`= 0 WHERE `Id`= 27008;

-- http://es.wowhead.com/quest=26550 (no funciona el inciensio)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0 WHERE `Id`= 26550;

-- http://es.wowhead.com/quest=11617 (no se pueden destruir las plataformas)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 11617;

-- http://es.wowhead.com/quest=11607 (no cuenta cuando liberas a los objetivos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0 WHERE `Id`= 11607;

-- http://es.wowhead.com/quest=12003 (no cuenta cuando exploras los edificios)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 12003;

-- http://es.wowhead.com/quest=11982 (no se puede descender  y las rocas no cuentan cuando las usas)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 11982;

-- http://es.wowhead.com/quest=12671 (solo queda usar el method)
UPDATE `quest_template`SET `Method`= 0 WHERE `Id`= 12671;

-- http://es.wowhead.com/quest=12592 (no se puede hablar con el NPC para dar inicio a la caza )
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12592;

-- http://es.wowhead.com/quest=12038 (no cuenta el uso del item para completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 12038;

-- http://es.wowhead.com/quest=11989 (no puedes completar la quest porque el uso de la espada no cuenta para la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 11989;

-- http://es.wowhead.com/quest=12864 (solo queda usar el method)
UPDATE `quest_template`SET `Method`= 0 WHERE `Id`= 12864;

-- http://es.wowhead.com/quest=28864 (no se peude escoltar a la caravana)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 28864;

-- http://es.wowhead.com/quest=26632 (no se puede completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 26632;

-- http://es.wowhead.com/quest=14491 (no funciona el uso de los tambores en los elementales)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 14491;

-- http://www.wowhead.com/quest=29364 (golpeas los objetivos pero no te completan la mision)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 29364;

-- http://es.wowhead.com/quest=12893 (no funciona la varita)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 12893;

-- http://es.wowhead.com/quest=29326 (no se peude hablar con trhall y no esta el NPC  al cual se debe entregar la mision)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 29326;
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(NULL, 54312, 1, 1, 1, 0, 0, 5349.953613, -3483.806152, 1569.767944, 3.258801, 300, 0, 0, 77490, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=11170 (no se peude montar el  murcielago y por consiguiente no se puede completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 11170;

-- http://es.wowhead.com/quest=10309 (el item no funciona , no invoca bien al atracador vil)
UPDATE `quest_template` SET `RequiredItemCount1`= 0 WHERE `Id`= 10309;

-- http://es.wowhead.com/quest=25303 (no se pueden activar los  objetivos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0, `RequiredNpcOrGoCount4`= 0   WHERE `Id`= 25303;

-- http://es.wowhead.com/quest=25159 (no funciona el polvo)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0  WHERE `Id`= 25159;

-- http://es.wowhead.com/quest=24766 (no funciona el spell en el mu�eco)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0  WHERE `Id`= 24766;

-- http://es.wowhead.com/quest=9923 (no se puede obtener la llave por que los moobs no la dropean)
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(17135, 25490, -23, 1, 0, 1, 1),
(17134, 25490, -23, 1, 0, 1, 1);

-- http://es.wowhead.com/quest=28195 (falta 1 objetivo de mision)
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(NULL, 47980, 1, 1, 1, 0, 0, -10880.238281, 900.982727, 18.585543, 6.120876, 120, 0, 0, 64496, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=12919 (no se puede completar lo de los 100 objetivos pero matando a los otros objetivos se completa la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0  WHERE `Id`= 12919;
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(NULL, 29895, 571, 1, 1, 0, 0, 5681.301270, -2120.394043, 234.628860, 0.635153, 120, 0, 0, 94000, 0, 0, 0, 0, 0 ),
(NULL, 29821, 571, 1, 1, 0, 0, 5562.873047, -2154.556641, 238.686035, 0.996351, 120, 0, 0, 44004, 0, 0, 0, 0, 0 ),
(NULL, 29872, 571, 1, 1, 0, 0, 6049.475098, -1995.422852, 234.244125, 3.026550, 120, 0, 0, 85000, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=28123 (el objetivo no essta y aparece en amistoso, ademas no tiene loot)
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(NULL, 47618, 0, 1, 1, 0, 0, -3045.850098, -5092.629883, 132.199005, 2.588150, 120, 0, 0, 85239, 0, 0, 0, 0, 0 );
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(47618, 63036, -100, 1, 0, 1, 1);

UPDATE `creature_template` SET `lootid`= 63036, `questItem1`= 63036 WHERE `entry`= 47618;

-- http://es.wowhead.com/quest=14469 ( no se puede cojer los objetivos)
UPDATE `quest_template` SET `RequiredItemCount1`= 0  WHERE `Id`= 14469;

-- http://es.wowhead.com/quest=12530 ( no funciona la varita, pero sacando al objetivo de su otra face sey matandolo se puede completar la quest)
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 28221;

-- http://es.wowhead.com/quest=27448 ( no funciona el evento de la caravana)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 27448;

-- http://es.wowhead.com/quest=27161 (el primer objetivo no se puede seleccionar ni ver, pero matando a lso otros se peude completar la quest))
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 27161;

-- http://es.wowhead.com/quest=27168 (no funciona el turibulo sagrado)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 27168;

-- http://es.wowhead.com/quest=11126 (no se puede hablar con los guardias para amtarlos)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 11126;

-- http://es.wowhead.com/quest=25328 (falta el contenedor de la llave)
INSERT INTO gameobject (guid, id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(NULL, 204580, 1, 1, 1, 5101.765137, -2053.120117, 1275.520752, 2.296078, 0, 0, 0, 0, 60, 100, 1 );

-- http://es.wowhead.com/quest=10070 (no cuenta cuando golpeas al mu�eco)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 10070;


-- http://es.wowhead.com/quest=13850 (no funciona el spell, y tampoco la cuenta aun colocandole el spell al objetivo manualmente)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 13850;

-- http://es.wowhead.com/npc=31268 (el vehiculo no tiene mana para poder usar sus hechizos y completar la quest)
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 13395;



