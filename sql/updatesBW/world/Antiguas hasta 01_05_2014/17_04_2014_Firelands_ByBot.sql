update `creature_onkill_reputation` set RewOnKillRepFaction1=1204 where RewOnKillRepFaction1=1178 AND creature_id in (52530, 53494, 52498, 52558, 52571, 52409, 53691, 53134, 53130, 53876, 54275, 53791, 53786, 53734, 53896, 53369, 53832, 54019, 53872, 54276, 52581, 53635, 52447, 53631, 52524, 53642, 54147, 53819, 54203, 53619, 53795, 53745, 53127, 53096, 54161, 53187, 53223, 53639, 53121, 53222, 53119, 54073, 53185, 53120, 53640, 53188, 53224, 53244, 52672, 52620, 53128, 53793, 53206, 53375, 53648, 53616, 54277, 53129, 53231, 53622, 53575, 52577, 52619, 53500, 54145, 54144, 54274, 54015, 53875, 53095, 53189, 53617, 54143, 53115, 53489, 54563, 53141, 53094, 53695, 53087, 53694, 53794, 53140, 53211, 52593, 53825, 53116, 53167, 53732, 53901, 53630, 53833, 53509, 53898);
UPDATE `creature_template` SET `unit_flags` = '0' , `unit_flags2` = '0', `faction_A` = '14' , `faction_H` = '14' WHERE `entry` = '11502';
UPDATE `creature_template` SET `faction_A`=16, `faction_H`=16 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=16 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=16, `faction_H`=16 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `faction_H`=16 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=9821 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=9821 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=9821 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=16871 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=16871 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=16871 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=20 WHERE  `entry`=52498 LIMIT 1;
UPDATE `creature_template` SET `attackpower`=46 WHERE  `entry`=52498 LIMIT 1;
UPDATE `creature_template` SET `lootid`=53576 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `lootid`=53577 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `lootid`=53578 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `flags_extra`=1 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `flags_extra`=1 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `flags_extra`=1 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `unit_class`=4 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `unit_class`=4 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `unit_class`=4 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=14 WHERE  `entry`=53599 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=14 WHERE  `entry`=53600 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=14, `faction_H`=14 WHERE  `entry`=53601 LIMIT 1;
UPDATE `creature_template` SET `faction_H`=14 WHERE  `entry`=53600 LIMIT 1;
UPDATE `creature_template` SET `faction_H`=14 WHERE  `entry`=53599 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `exp`=3 WHERE  `entry`=53599 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=2000 WHERE  `entry`=53599 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=4000 WHERE  `entry`=53599 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `exp`=3, `faction_A`=14, `faction_H`=14 WHERE  `entry`=53579 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=14 WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=14, `faction_H`=14 WHERE  `entry`=53581 LIMIT 1;
UPDATE `creature_template` SET `faction_H`=14 WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=20 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=20 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=20 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=11 WHERE  `entry`=52498 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=13 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=12 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=13 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=4000, `maxdmg`=6000 WHERE  `entry`=52498 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=4000 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=4000 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=4000 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=6000 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=6000 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=6000 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=6 WHERE  `entry`=52498 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=8 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=7 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=8 WHERE  `entry`=53578 LIMIT 1;DELETE FROM `spell_script_names` WHERE `spell_id` IN(99263, 99256, 100230, 100231, 100232);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (99263, 'spell_vital_flame');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (99256, 'spell_torment');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (100230, 'spell_torment');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (100231, 'spell_torment');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (100232, 'spell_torment');

-- baleroc hp
UPDATE `creature_template` SET `exp` = '3' WHERE `entry` = '53588';
UPDATE `creature_template` SET `exp` = '3' WHERE `entry` = '53589'; 
UPDATE `creature_template` SET `Health_mod` = '490' WHERE `entry` = '53494'; 
UPDATE `creature_template` SET `Health_mod` = '1552' WHERE `entry` = '53587'; 
UPDATE `creature_template` SET `Health_mod` = '921' WHERE `entry` = '53588'; 
UPDATE `creature_template` SET `Health_mod` = '2580' WHERE `entry` = '53589'; 

-- shannox hp
UPDATE `creature_template` SET `Health_mod` = '280' WHERE `entry` = '53691';
UPDATE `creature_template` SET `Health_mod` = '390' WHERE `entry` = '54079'; 
UPDATE `creature_template` SET `Health_mod` = '950' WHERE `entry` = '53979'; 
UPDATE `creature_template` SET `Health_mod` = '1330' WHERE `entry` = '54080'; 
-- rageface hp
UPDATE `creature_template` SET `Health_mod` = '112' WHERE `entry` = '53695'; 
UPDATE `creature_template` SET `Health_mod` = '392' WHERE `entry` = '53981'; 
-- riplimb hp
UPDATE `creature_template` SET `Health_mod` = '112' WHERE `entry` = '53694'; 
UPDATE `creature_template` SET `Health_mod` = '392' WHERE `entry` = '53980'; 



Update `creature_template` SET health_mod=911 WHERE entry=52558; -- gi� alzato
delete from `areatrigger_scripts` where `entry` =  5770;
INSERT INTO `areatrigger_scripts` (`entry`, `ScriptName`) VALUES ('5770', 'at_sulfuron_keep');
UPDATE `creature_template` SET `ScriptName` = 'boss_ragnaros_cata' WHERE `entry` = '52409';
delete from `creature_equip_template` where `entry` = '52409';
INSERT INTO `creature_equip_template` (`entry`, `id`, `itemEntry1`) VALUES ('52409', '1', '99173'); 

delete from `spell_proc_event` where `entry` = 99252;
INSERT INTO `spell_proc_event` (`entry`, `procFlags`, `CustomChance`, `Cooldown`) VALUES ('99252', '33554431', '100', '5');DELETE FROM `spell_proc_event` WHERE `entry` IN (96879, 96924);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `CustomChance`, `Cooldown`) VALUES 
(96924, 0, 0, 0, 0, 0, 0x00000014, 0x0000002, 100, 0),
(96879, 0, 0, 0, 0, 0, 0x00008000, 0, 100, 0);

DELETE FROM `spell_script_names` WHERE `spell_id` IN (96880, 96934);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES 
(96934, 'spell_gen_blessing_of_khazgoroth'),
(96880, 'spell_gen_tipping_of_scales');DELETE FROM `spell_script_names` WHERE `spell_id` IN(99263, 99256);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (99263, 'spell_vital_flame');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (99256, 'spell_torment');

UPDATE `creature_template` SET `mechanic_immune_mask`=635387903 WHERE (`entry`=53494);DELETE FROM `spell_script_names` WHERE `spell_id` IN(99353, -99353);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (99353, 'spell_decimation_strike');UPDATE `spell_proc_event` SET `procFlags` = 16777212 WHERE `entry` = 99252;
DELETE FROM `reference_loot_template` WHERE `entry` = 53494;
INSERT INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES 
(53494, 71343, 0, 1, 1, 1, 1),
(53494, 71345, 0, 1, 1, 1, 1),
(53494, 71314, 0, 1, 1, 1, 1),
(53494, 71341, 0, 1, 1, 1, 1),
(53494, 71340, 0, 1, 1, 1, 1),
(53494, 71315, 0, 1, 1, 1, 1),
(53494, 71342, 0, 1, 1, 1, 1),
(53494, 70916, 0, 1, 1, 1, 1),
(53494, 70917, 0, 1, 1, 1, 1),
(53494, 68982, 0, 1, 1, 1, 1),
(53494, 71323, 0, 1, 1, 1, 1),
(53494, 71312, 0, 1, 1, 1, 1),
(53494, 70915, 0, 1, 1, 1, 1),
(53494, 71776, 0, 1, 1, 1, 1),
(53494, 71775, 0, 1, 1, 1, 1),
(53494, 71782, 0, 1, 1, 1, 1),
(53494, 71780, 0, 1, 1, 1, 1),
(53494, 71779, 0, 1, 1, 1, 1),
(53494, 71785, 0, 1, 0, 1, 1),
(53494, 71787, 0, 1, 1, 1, 1);

DELETE FROM `creature_loot_template` WHERE entry IN (53494, 53587);
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES 
(53494, 1, 100, 1, 1, -53494, 3),
(53494, 69237, 100, 1, 0, 1, 2),
(53587, 1, 100, 1, 1, -53494, 6),
(53587, 69237, 100, 1, 0, 1, 2);


DELETE FROM `creature_onkill_currency` WHERE  `creature_id`=53494;
INSERT INTO `creature_onkill_currency` (`creature_id`, `CurrencyId1`, `CurrencyCount1`) VALUES (53494, 396, 120);

DELETE FROM `creature_onkill_currency` WHERE  `creature_id`=53587;
INSERT INTO `creature_onkill_currency` (`creature_id`, `CurrencyId1`, `CurrencyCount1`) VALUES (53587, 396, 120);

DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53494;
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53494, 1204, 7, 400);

DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53587;
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53587, 1204, 7, 400);

UPDATE `creature_template` SET `exp`=3 WHERE  `entry`=53587;
UPDATE `creature_template` SET `Health_mod`=1164 WHERE  `entry`=53587;
UPDATE `creature_template` SET `Health_mod`=367.5 WHERE  `entry`=53494;


UPDATE `creature_template` SET `lootid`=53587 WHERE  `entry`=53587;DELETE FROM `creature` WHERE `id` = 52498;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES 
(NULL, 52498, 720, 15, 1, 0, 0, 41.8449, 399.831, 74.0416, 4.25389, 3000000, 0, 0, 83658808, 0, 0, 0, 0, 0);

UPDATE `creature_template` SET `difficulty_entry_1`=53576, `difficulty_entry_2`=53577, `difficulty_entry_3`=53578, `flags_extra`=1, `ScriptName`='boss_bethtilac' WHERE  `entry`=52498 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=88, `maxlevel`=88, `exp`=3 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=88, `maxlevel`=88, `exp`=3 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=88, `maxlevel`=88, `exp`=3 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `mechanic_immune_mask`=635387903 WHERE  `entry`=53578 LIMIT 1;
UPDATE `creature_template` SET `mechanic_immune_mask`=635387903 WHERE  `entry`=53577 LIMIT 1;
UPDATE `creature_template` SET `mechanic_immune_mask`=635387903 WHERE  `entry`=53576 LIMIT 1;
UPDATE `creature_template` SET `Health_mod`=206 WHERE  `entry`=52498 LIMIT 1;

UPDATE `creature_template` SET `difficulty_entry_1`=53599 WHERE  `entry`=53642 LIMIT 1;
UPDATE `creature_template` SET `difficulty_entry_2`=53600, `difficulty_entry_3`=53601 WHERE  `entry`=53642 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `exp`=3 WHERE  `entry`=53643 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `exp`=3 WHERE  `entry`=53600 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `exp`=3 WHERE  `entry`=53601 LIMIT 1;
UPDATE `creature_template` SET `Health_mod`=4 WHERE  `entry`=53600 LIMIT 1;
UPDATE `creature_template` SET `Health_mod`=12 WHERE  `entry`=53601 LIMIT 1;
UPDATE `creature_template` SET `AIName`='', `ScriptName`='npc_cinderweb_spinner' WHERE  `entry`=53642 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=2000, `maxdmg`=4000 WHERE  `entry`=53642 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=2000 WHERE  `entry`=53643 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=2000 WHERE  `entry`=53600 LIMIT 1;
UPDATE `creature_template` SET `mindmg`=2000 WHERE  `entry`=53601 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=4000 WHERE  `entry`=53643 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=4000 WHERE  `entry`=53600 LIMIT 1;
UPDATE `creature_template` SET `maxdmg`=4000 WHERE  `entry`=53601 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=6 WHERE  `entry`=53643 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=5 WHERE  `entry`=53600 LIMIT 1;
UPDATE `creature_template` SET `dmg_multiplier`=7 WHERE  `entry`=53601 LIMIT 1;
UPDATE `creature_template` SET `ScriptName`='npc_spirderweb_filament' WHERE  `entry`=53082 LIMIT 1;
UPDATE `creature_template` SET `npcflag`=1 WHERE  `entry`=53082 LIMIT 1;
UPDATE `creature_template` SET `modelid2`=0 WHERE  `entry`=53237 LIMIT 1;
UPDATE `creature_template` SET `unit_class`=4 WHERE  `entry`=52498 LIMIT 1;
UPDATE `creature_template` SET `difficulty_entry_1`=53579, `difficulty_entry_2`=53580, `difficulty_entry_3`=53581 WHERE  `entry`=53631 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85 WHERE  `entry`=53632 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85 WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85 WHERE  `entry`=53581 LIMIT 1;
UPDATE `creature_template` SET `maxlevel`=85 WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `maxlevel`=85 WHERE  `entry`=53632 LIMIT 1;
UPDATE `creature_template` SET `exp`=3 WHERE  `entry`=53632 LIMIT 1;
UPDATE `creature_template` SET `exp`=3 WHERE  `entry`=53581 LIMIT 1;
UPDATE `creature_template` SET `exp`=3 WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `ScriptName`='npc_cinderweb_spiderling' WHERE  `entry`=53631 LIMIT 1;
UPDATE `creature_template` SET `AIName`='' WHERE  `entry`=53631 LIMIT 1;
UPDATE `creature_template` SET `AIName`='' WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `AIName`='' WHERE  `entry`=53581 LIMIT 1;
UPDATE `creature_template` SET `movementId`=106 WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `AIName`='' WHERE  `entry`=53579 LIMIT 1;
UPDATE `creature_template` SET `Health_mod`=1.54859 WHERE  `entry`=53579 LIMIT 1;
UPDATE `creature_template` SET `Health_mod`=1.54859 WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `Health_mod`=1.54859 WHERE  `entry`=53581 LIMIT 1;
UPDATE `creature_template` SET `Health_mod`=3 WHERE  `entry`=53579 LIMIT 1;
UPDATE `creature_template` SET `Health_mod`=3 WHERE  `entry`=53580 LIMIT 1;
UPDATE `creature_template` SET `difficulty_entry_1`=53582, `difficulty_entry_2`=53583, `difficulty_entry_3`=53584, `exp`=3, `faction_A`=16, `faction_H`=16 WHERE  `entry`=52581 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85, `maxlevel`=85, `exp`=3 WHERE  `entry`=53582 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85 WHERE  `entry`=53583 LIMIT 1;
UPDATE `creature_template` SET `minlevel`=85 WHERE  `entry`=53584 LIMIT 1;
UPDATE `creature_template` SET `maxlevel`=85 WHERE  `entry`=53583 LIMIT 1;
UPDATE `creature_template` SET `maxlevel`=85 WHERE  `entry`=53584 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=16, `faction_H`=16 WHERE  `entry`=53582 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=16 WHERE  `entry`=53583 LIMIT 1;
UPDATE `creature_template` SET `faction_A`=16 WHERE  `entry`=53584 LIMIT 1;
UPDATE `creature_template` SET `faction_H`=16 WHERE  `entry`=53583 LIMIT 1;
UPDATE `creature_template` SET `faction_H`=16 WHERE  `entry`=53584 LIMIT 1;
UPDATE `creature_template` SET `exp`=3 WHERE  `entry`=53582 LIMIT 1;
UPDATE `creature_template` SET `exp`=3 WHERE  `entry`=53583 LIMIT 1;
UPDATE `creature_template` SET `exp`=3 WHERE  `entry`=53584 LIMIT 1;
UPDATE `creature_template` SET `ScriptName`='npc_cinderweb_drone' WHERE  `entry`=52581 LIMIT 1;

DELETE FROM `reference_loot_template` WHERE `entry` = 52498;
INSERT INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES 
(52498, 71780, 0, 1, 1, 1, 1),
(52498, 71032, 0, 1, 1, 1, 1),
(52498, 71038, 0, 1, 1, 1, 1),
(52498, 71042, 0, 1, 1, 1, 1),
(52498, 68981, 0, 1, 1, 1, 1),
(52498, 71043, 0, 1, 1, 1, 1),
(52498, 71775, 0, 1, 1, 1, 1),
(52498, 71782, 0, 1, 1, 1, 1),
(52498, 71041, 0, 1, 1, 1, 1),
(52498, 70922, 0, 1, 1, 1, 1),
(52498, 71039, 0, 1, 1, 1, 1),
(52498, 71030, 0, 1, 1, 1, 1),
(52498, 71785, 0, 1, 1, 1, 1),
(52498, 71776, 0, 1, 1, 1, 1),
(52498, 71787, 0, 1, 1, 1, 1),
(52498, 71040, 0, 1, 1, 1, 1),
(52498, 71031, 0, 1, 1, 1, 1),
(52498, 71044, 0, 1, 1, 1, 1),
(52498, 70914, 0, 1, 0, 1, 1),
(52498, 71779, 0, 1, 0, 1, 1),
(52498, 71029, 0, 1, 1, 1, 1);

DELETE FROM `creature_loot_template` WHERE entry IN  (52498, 53576);
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES 
(52498, 1, 100, 1, 1, -52498, 3),
(52498, 69237, 100, 1, 0, 1, 2),
(53576, 1, 100, 1, 1, -52498, 6),
(53576, 69237, 100, 1, 0, 1, 2);

DELETE FROM `creature_onkill_currency` WHERE  `creature_id`=52498;
INSERT INTO `creature_onkill_currency` (`creature_id`, `CurrencyId1`, `CurrencyCount1`) VALUES (52498, 396, 120);

DELETE FROM `creature_onkill_currency` WHERE  `creature_id`=53576;
INSERT INTO `creature_onkill_currency` (`creature_id`, `CurrencyId1`, `CurrencyCount1`) VALUES (53576, 396, 120);

delete from creature where id IN (53490, 53492);DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN(-99256, -100230, -100231, -100232);
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES (-99256, 99257, 'Torment fading applies Tormented');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES (-100230, 99402, 'Torment fading applies Tormented');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES (-100231, 99403, 'Torment fading applies Tormented');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES (-100232, 99404, 'Torment fading applies Tormented');

UPDATE `creature_template` SET `difficulty_entry_1`=53587, `difficulty_entry_2`=53588, `difficulty_entry_3`=53589, `mechanic_immune_mask`=650854369 WHERE (`entry`=53494);

UPDATE `creature_template` SET `minlevel`=88, `maxlevel`=88, `exp`=3, `faction_A`=14, `faction_H`=14,  `speed_walk`=3.2, `speed_run`=2, `mindmg`=1225.5, `maxdmg`=1544.7, `attackpower`=1651, `baseattacktime`=2000, `rangeattacktime`=2000, `unit_flags`=32768,`minrangedmg`=1160.9, `maxrangedmg`=1328.1, `rangedattackpower`=225, `Armor_mod`=11977, `mechanic_immune_mask`=635387903,  `flags_extra`=1, `ScriptName`='boss_baleroc' WHERE (`entry`=53587);


UPDATE `creature_template` SET `modelid1`=11686, `modelid2`=0, `minlevel`=85, `maxlevel`=85, `faction_A`=14, `faction_H`=14, `type_flags`=0, `ScriptName`='npc_shard_of_torment' WHERE (`entry`=53495);

DELETE FROM `spell_script_names` WHERE `spell_id` IN(99516, 99517);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (99516, 'spell_countdown');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (99517, 'spell_countdown_tick');
UPDATE `creature_template` SET `mindmg`=7000, `maxdmg`=11000, `dmg_multiplier`=4 WHERE  `entry` IN (53115, 53094, 53095, 53141, 53119, 53121, 53120, 53206, 53167, 53134, 53102, 53185, 53188, 53187, 54161, 53224, 53222, 53223, 53119, 54073, 53642, 53635);DELETE FROM `creature` WHERE `id` IN (53520,53375,53698,53693,53487,53691,52530,53369,53680,53734,53896,53900) AND `map`=720;
DELETE FROM `gameobject` WHERE `id` IN (209036,208966,201722,209035) AND `map`=720;

UPDATE `creature_template` SET `flags_extra`=`flags_extra`|0x80 WHERE `entry` IN (53723,53154,53789,53792,53787,53788);

UPDATE `creature_template` SET `minlevel`=88,`maxlevel`=88,`exp`=3,`faction_A`=16,`faction_H`=16,`speed_walk`=3.2,`speed_run`=2,`baseattacktime`=2000,`rangeattacktime`=2000,`unit_class`=2,`unit_flags`=0x2008140,`unit_flags2`=0x8000800,`VehicleId`=1673,`HoverHeight`=14,`mana_mod_extra`=1.72414 WHERE `entry` IN (52530,54044,54045,54046);
UPDATE `creature_template` SET `minlevel`=87,`maxlevel`=87,`exp`=3,`faction_A`=16,`faction_H`=16,`speed_walk`=3.2,`speed_run`=2,`baseattacktime`=2000,`rangeattacktime`=2000,`unit_flags`=0x8000,`unit_flags2`=0x800,`VehicleId`=1695 WHERE `entry` IN (54056,54057); -- Blazing Monstrosity
UPDATE `creature_template` SET `minlevel`=87,`maxlevel`=87,`exp`=3,`faction_A`=16,`faction_H`=16,`speed_walk`=1,`speed_run`=1,`baseattacktime`=2000,`rangeattacktime`=2000,`unit_flags`=0x8000,`unit_flags2`=0x8800 WHERE `entry` IN (53795,54059); -- Egg Pile
UPDATE `creature_template` SET `spell1`=100076,`spell2`=100080,`spell3`=100078,`spell4`=100082 WHERE `entry`=53789;
UPDATE `creature_template` SET `spell1`=100090,`spell2`=100089,`spell3`=100091,`spell4`=100092 WHERE `entry`=53792;
UPDATE `creature_template` SET `minlevel`=85,`maxlevel`=85,`exp`=3,`faction_A`=14,`faction_H`=14,`speed_walk`=1.2,`speed_run`=0.428571,`unit_flags`=0x2000000,`unit_flags2`=0x2000800 WHERE `entry` IN (53787,53788); -- Molten Barrage
UPDATE `creature_template` SET `minlevel`=85,`maxlevel`=85,`exp`=3,`faction_A`=16,`faction_H`=16,`speed_walk`=3.2,`speed_run`=2,`unit_flags`=0x8000,`unit_flags2`=0x800,`baseattacktime`=2000,`rangeattacktime`=2000 WHERE `entry` IN (53794,54058); -- Smouldering Hatchling
UPDATE `creature_template` SET `minlevel`=86,`maxlevel`=86,`exp`=3,`faction_A`=16,`faction_H`=16,`speed_walk`=1,`speed_run`=1.14286,`unit_flags`=0,`unit_flags2`=0x800,`baseattacktime`=2000,`rangeattacktime`=2000,`unit_class`=8,`dynamicflags`=0 WHERE `entry` IN (53793,53973); -- Harbinger of Flame

UPDATE `creature_template` SET `difficulty_entry_1`=54044,`difficulty_entry_2`=54045,`difficulty_entry_3`=54046 WHERE `entry`=52530; -- Alysrazor
UPDATE `creature_template` SET `difficulty_entry_1`=54056 WHERE `entry`=53786; -- Blazing Monstrosity
UPDATE `creature_template` SET `difficulty_entry_1`=54057 WHERE `entry`=53791; -- Blazing Monstrosity
UPDATE `creature_template` SET `difficulty_entry_1`=54059 WHERE `entry`=53795; -- Egg Pile
UPDATE `creature_template` SET `difficulty_entry_1`=54058 WHERE `entry`=53794; -- Smouldering Hatchling
UPDATE `creature_template` SET `difficulty_entry_1`=53973 WHERE `entry`=53793; -- Harbinger of Flame

DELETE FROM `creature_template_addon` WHERE `entry` IN (53786,53791,54056,54057,53794,54058,54276,54019,53224,53102,52530,54044,54045,54046,53900,53680,53520,53693,53698);
INSERT INTO `creature_template_addon` (`entry`, `mount`, `bytes1`, `bytes2`, `auras`) VALUES
(53786, 0, 0x0, 0x1, '100712 99480'), -- Blazing Monstrosity - Fire Hawk Smoke Cosmetic, Sleep (Ultra-High Priority)
(53791, 0, 0x0, 0x1, '100712 99480'), -- Blazing Monstrosity - Fire Hawk Smoke Cosmetic, Sleep (Ultra-High Priority)
(54056, 0, 0x0, 0x1, '100712 99480'), -- Blazing Monstrosity - Fire Hawk Smoke Cosmetic, Sleep (Ultra-High Priority)
(54057, 0, 0x0, 0x1, '100712 99480'), -- Blazing Monstrosity - Fire Hawk Smoke Cosmetic, Sleep (Ultra-High Priority)
(53794, 0, 0x0, 0x1, '100712'), -- Smouldering Hatchling - Fire Hawk Smoke Cosmetic
(54058, 0, 0x0, 0x1, '100712'), -- Smouldering Hatchling - Fire Hawk Smoke Cosmetic
(54276, 0, 0x0, 0x1, '101112'), -- Cinderslither Snake - Cinderslither Aura
(54019, 0, 0x0, 0x1, '100556'), -- Captive Druid of the Talon - Smouldering Roots
(53224, 0, 0x0, 0x1, '100743'), -- Flamewaker Taskmaster - Aura of Indomitability
(53102, 0, 0x3000000, 0x1, '100712'), -- Inferno Hawk - Fire Hawk Smoke Cosmetic
(52530, 0, 0x0, 0x1, '100712'), -- Alysrazor - Fire Hawk Smoke Cosmetic
(54044, 0, 0x0, 0x1, '100712'), -- Alysrazor - Fire Hawk Smoke Cosmetic
(54045, 0, 0x0, 0x1, '100712'), -- Alysrazor - Fire Hawk Smoke Cosmetic
(54046, 0, 0x0, 0x1, '100712'), -- Alysrazor - Fire Hawk Smoke Cosmetic
(53900, 0, 0x3000000, 0x1, '100712'), -- Blazing Broodmother - Fire Hawk Smoke Cosmetic
(53680, 0, 0x3000000, 0x1, '100712'), -- Blazing Broodmother - Fire Hawk Smoke Cosmetic
(53520, 0, 0x0, 0x1, '99327'), -- Plump Lava Worm - Fire Worm Cosmetic
(53693, 0, 0x0, 0x1, '99793'), -- Fiery Vortex - Fiery Vortex
(53698, 0, 0x0, 0x1, '99817'); -- Fiery Tornado - Fiery Tornado

DELETE FROM `vehicle_template_accessory` WHERE `entry` IN (53786,53791);
INSERT INTO `vehicle_template_accessory` (`entry`, `accessory_entry`, `minion`, `description`, `summontype`, `summontimer`) VALUES
(53786,53789,1,'Blazing Monstrosity',5,0),
(53791,53792,1,'Blazing Monstrosity',5,0);

DELETE FROM `npc_spellclick_spells` WHERE `npc_entry` IN (53786,53791,53789,53792);
INSERT INTO `npc_spellclick_spells` (`npc_entry`,`spell_id`,`cast_flags`) VALUES
(53786,98509,1),
(53791,98509,1),
(53789,93970,1),
(53792,93970,1);

DELETE FROM `creature_text` WHERE `entry`=53795;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(53795, 0, 0, 'The Molten Eggs begin to crack and splinter!', 41, 0, 100, 0, 0, 0, 'Molten Egg - EMOTE_CRACKING_EGGS');

-- ScriptNames and conditions
UPDATE `instance_template` SET `script`='instance_firelands' WHERE `map`=720;
UPDATE `creature_template` SET `ScriptName`='npc_harbinger_of_flame' WHERE `entry`=53793;
UPDATE `creature_template` SET `ScriptName`='npc_blazing_monstrosity' WHERE `entry` IN (53786,53791);
UPDATE `creature_template` SET `ScriptName`='npc_molten_barrage' WHERE `entry` IN (53787,53788);
UPDATE `creature_template` SET `ScriptName`='npc_egg_pile' WHERE `entry`=53795;

DELETE FROM `spell_script_names` WHERE `ScriptName`='spell_alysrazor_cosmetic_egg_xplosion';
DELETE FROM `spell_script_names` WHERE `ScriptName`='spell_alysrazor_turn_monstrosity';
DELETE FROM `spell_script_names` WHERE `ScriptName`='spell_alysrazor_aggro_closest';
DELETE FROM `spell_script_names` WHERE `ScriptName`='spell_alysrazor_fieroblast';
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(100099,'spell_alysrazor_cosmetic_egg_xplosion'),
(100076,'spell_alysrazor_turn_monstrosity'),
(100078,'spell_alysrazor_turn_monstrosity'),
(100080,'spell_alysrazor_turn_monstrosity'),
(100082,'spell_alysrazor_turn_monstrosity'),
(100089,'spell_alysrazor_turn_monstrosity'),
(100090,'spell_alysrazor_turn_monstrosity'),
(100091,'spell_alysrazor_turn_monstrosity'),
(100092,'spell_alysrazor_turn_monstrosity'),
(100462,'spell_alysrazor_aggro_closest'),
(100094,'spell_alysrazor_fieroblast'),
(101223,'spell_alysrazor_fieroblast'),
(101294,'spell_alysrazor_fieroblast'),
(101295,'spell_alysrazor_fieroblast'),
(101296,'spell_alysrazor_fieroblast');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry` IN (100558,100071,100074,100070,100076,100078,100080,100082,100089,100090,100091,100092);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`,`ErrorType`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(13,3,100558,0,0,31,0,3,54019,0,0,0,0,'','Sacrifice to the Flame - Captive Druid of the Talon'),
(13,1,100071,0,0,31,0,3,53787,0,0,0,0,'','Molten Barrage'),
(13,1,100074,0,0,31,0,3,53788,0,0,0,0,'','Molten Barrage'),
(13,2,100070,0,0,31,0,3,53786,0,0,0,0,'','Molten Barrage - Blazing Monstrosity'),
(13,2,100070,0,1,31,0,3,53791,0,0,0,0,'','Molten Barrage - Blazing Monstrosity'),
(13,2,100070,0,2,31,0,3,53793,0,0,0,0,'','Molten Barrage - Harbinger of Flame'),
(13,2,100070,0,3,31,0,3,53795,0,0,0,0,'','Molten Barrage - Egg Pile'),
(13,2,100070,0,4,31,0,3,53794,0,0,0,0,'','Molten Barrage - Smouldering Hatchling'),
(13,1,100076,0,0,31,0,3,53787,0,0,0,0,'','Left-Side Smack - Molten Barrage'),
(13,2,100076,0,1,31,0,3,53786,0,0,0,0,'','Left-Side Smack - Blazing Monstrosity'),
(13,1,100078,0,0,31,0,3,53787,0,0,0,0,'','Right-Side Smack - Molten Barrage'),
(13,2,100078,0,1,31,0,3,53786,0,0,0,0,'','Right-Side Smack - Blazing Monstrosity'),
(13,1,100080,0,0,31,0,3,53787,0,0,0,0,'','Head Bonk - Molten Barrage'),
(13,2,100080,0,1,31,0,3,53786,0,0,0,0,'','Head Bonk - Blazing Monstrosity'),
(13,1,100082,0,0,31,0,3,53787,0,0,0,0,'','Tickle - Molten Barrage'),
(13,2,100082,0,1,31,0,3,53786,0,0,0,0,'','Tickle - Blazing Monstrosity'),
(13,1,100089,0,0,31,0,3,53788,0,0,0,0,'','Head Bonk - Molten Barrage'),
(13,2,100089,0,1,31,0,3,53791,0,0,0,0,'','Head Bonk - Blazing Monstrosity'),
(13,1,100090,0,0,31,0,3,53788,0,0,0,0,'','Left-Side Smack - Molten Barrage'),
(13,2,100090,0,1,31,0,3,53791,0,0,0,0,'','Left-Side Smack - Blazing Monstrosity'),
(13,1,100091,0,0,31,0,3,53788,0,0,0,0,'','Right-Side Smack - Molten Barrage'),
(13,2,100091,0,1,31,0,3,53791,0,0,0,0,'','Right-Side Smack - Blazing Monstrosity'),
(13,1,100092,0,0,31,0,3,53788,0,0,0,0,'','Tickle - Molten Barrage'),
(13,2,100092,0,1,31,0,3,53791,0,0,0,0,'','Tickle - Blazing Monstrosity');
Update `creature_template` SET `exp`=3 WHERE entry in (53691, 52409, 52571, 52558, 52498, 53494, 52530);
Update `creature_template` SET health_mod=1130 WHERE entry=53691;
Update `creature_template` SET health_mod=2443 WHERE entry=52409;
Update `creature_template` SET health_mod=4276 WHERE entry=52571;
Update `creature_template` SET health_mod=775 WHERE entry=52558;
Update `creature_template` SET health_mod=974 WHERE entry=52498;
Update `creature_template` SET health_mod=1935 WHERE entry=53494;
Update `creature_template` SET health_mod=2295 WHERE entry=52530;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=71640;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=71365;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=70929;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=71367;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=68972;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=68915;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=71359;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=71362;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=71361;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=71360;
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=0.6 WHERE `item`=71366;Update `creature_template` SET `mechanic_immune_mask`=617299799 WHERE entry in (53691, 52409, 52571, 52558, 52498, 53494, 52530);
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54402;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54401;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54367;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54348;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54299;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54277;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54276;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54275;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54274;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54161;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54143;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54073;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54020;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54019;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=54015;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53986;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53952;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53914;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53910;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53901;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53876;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53875;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53872;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53795;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53793;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53791;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53786;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53732;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53729;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53723;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53695;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53694;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53648;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53642;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53640;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53639;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53635;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53631;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53619;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53617;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53616;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53585;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53575;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53545;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53529;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53494;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53492;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53490;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53488;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53485;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53474;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53435;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53433;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53395;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53393;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53244;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53237;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53231;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53224;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53223;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53222;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53209;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53206;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53188;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53187;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53186;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53185;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53178;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53167;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53158;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53154;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53141;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53140;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53134;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53130;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53129;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53128;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53127;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53121;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53120;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53119;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53116;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53115;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53102;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53096;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53095;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53094;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=52672;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=52659;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=52571;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=52558;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=52498;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=52409;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=47242;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=45979;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=42098;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=35592;
DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=15214;
-- shannox

DELETE FROM `creature_onkill_reputation` WHERE  `creature_id`=53691;


INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54402, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54401, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54367, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54348, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54299, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54277, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54276, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54275, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54274, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54161, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54143, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54073, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54020, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54019, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (54015, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53986, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53952, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53914, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53910, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53901, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53876, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53875, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53872, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53795, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53793, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53791, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53786, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53732, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53729, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53723, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53695, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53694, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53648, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53642, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53640, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53639, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53635, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53631, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53619, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53617, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53616, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53585, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53575, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53545, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53529, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53494, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53492, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53490, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53488, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53485, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53474, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53435, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53433, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53395, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53393, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53244, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53237, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53231, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53224, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53223, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53222, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53209, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53206, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53188, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53187, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53186, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53185, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53178, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53167, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53158, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53154, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53141, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53140, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53134, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53130, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53129, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53128, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53127, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53121, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53120, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53119, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53116, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53115, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53102, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53096, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53095, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53094, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (52672, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (52659, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (52571, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (52558, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (52498, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (52409, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (47242, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (45979, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (42098, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (35592, 1204, 5, 30);
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (15214, 1204, 5, 30);
-- shannox
INSERT INTO `creature_onkill_reputation` (`creature_id`, `RewOnKillRepFaction1`, `MaxStanding1`, `RewOnKillRepValue1`) VALUES (53691, 1204, 7, 250);-- dogs template and everything
UPDATE `creature_template` SET `difficulty_entry_1` = '53981', `difficulty_entry_2` = '54075', `difficulty_entry_3` = '54076' WHERE `entry` = '53695'; 
UPDATE `creature_template` SET `difficulty_entry_1` = '53980', `difficulty_entry_2` = '54077', `difficulty_entry_3` = '54078' WHERE `entry` = '53694'; 
UPDATE `creature_template` SET `minlevel` = '87' , `maxlevel` = '87' WHERE `entry` IN (53981, 54075, 54076, 53980, 54077, 54078); 
UPDATE `creature_template` SET `exp` = '3' WHERE `entry` IN (53981, 54075, 54076, 53980, 54077, 54078); 
UPDATE `creature_template` SET `faction_A` = '2109', `faction_H` = '2109' WHERE `entry` IN (53981, 54075, 54076, 53980, 54077, 54078); 
UPDATE `creature_template` SET `speed_walk` = '2.8', `speed_run` = '1.57143' WHERE `entry` IN (53981, 54075, 54076, 53980, 54077, 54078); 
UPDATE `creature_template` SET `mindmg` = '1811.5', `maxdmg` = '2358.5', `attackpower` = '1613', `baseattacktime` = '1000' WHERE `entry` IN (53981, 54075, 54076);
UPDATE `creature_template` SET `mindmg` = '4375.5', `maxdmg` = '5687', `attackpower` = '1613', `baseattacktime` = '1000' WHERE `entry` IN (53694, 53980, 54077, 54078); 
UPDATE `creature_template` SET `dynamicflags` = '12' WHERE `entry` IN (53981, 54075, 54076, 53980, 54077, 54078); 
UPDATE `creature_template` SET `unit_flags` = '32768' WHERE `entry` IN (53981, 54075, 54076, 53980, 54077, 54078); 
UPDATE `creature_template` SET `Armor_mod` = '11977' WHERE `entry` IN (53981, 54075, 54076, 53980, 54077, 54078); 
UPDATE `creature_template` SET `mechanic_immune_mask` = '635387903' WHERE `entry` IN (53981, 54075, 54076, 53980, 54077, 54078); 
UPDATE `creature_template` SET `dmg_multiplier` = '8' WHERE `entry` IN (53981, 53980); 
UPDATE `creature_template` SET `dmg_multiplier` = '10' WHERE `entry` IN (54075, 54077); 
UPDATE `creature_template` SET `dmg_multiplier` = '11' WHERE `entry` IN (54076, 54078); 

-- shannox melee damage
UPDATE `world`.`creature_template` SET `dmg_multiplier` = '40' WHERE `entry` = '53691'; 
UPDATE `world`.`creature_template` SET `dmg_multiplier` = '44' WHERE `entry` = '53979'; 
UPDATE `world`.`creature_template` SET `dmg_multiplier` = '50' WHERE `entry` = '54079'; 
UPDATE `world`.`creature_template` SET `dmg_multiplier` = '55' WHERE `entry` = '54080'; 

-- traps update
UPDATE `creature_template` SET `minlevel` = '88' , `maxlevel` = '88' WHERE `entry` = '53713'; 
UPDATE `creature_template` SET `minlevel` = '88' , `maxlevel` = '88' WHERE `entry` = '53724'; DELETE FROM `creature_loot_template` WHERE entry = 53979;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES 
(53979, 1, 100, 1, 1, -53691, 6),
(53979, 69237, 100, 1, 0, 1, 2);

DELETE FROM `creature_onkill_currency` WHERE creature_id  IN(53979, 53691);
INSERT INTO `creature_onkill_currency` (`creature_id`, `CurrencyId1`, `CurrencyCount1`) VALUES 
(53979, 396, 120),
(53691, 396, 120);DELETE FROM `reference_loot_template` WHERE `entry` = 53691;
INSERT INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES 
(53691, 70913, 0, 1, 1, 1, 1),
(53691, 71013, 0, 1, 1, 1, 1),
(53691, 71014, 0, 1, 1, 1, 1),
(53691, 71018, 0, 1, 1, 1, 1),
(53691, 71019, 0, 1, 1, 1, 1),
(53691, 71020, 0, 1, 1, 1, 1),
(53691, 71021, 0, 1, 1, 1, 1),
(53691, 71022, 0, 1, 1, 1, 1),
(53691, 71023, 0, 1, 1, 1, 1),
(53691, 71024, 0, 1, 1, 1, 1),
(53691, 71025, 0, 1, 1, 1, 1),
(53691, 71026, 0, 1, 1, 1, 1),
(53691, 71028, 0, 1, 1, 1, 1),
(53691, 71775, 0, 1, 1, 1, 1),
(53691, 71776, 0, 1, 1, 1, 1),
(53691, 71779, 0, 1, 1, 1, 1),
(53691, 71780, 0, 1, 1, 1, 1),
(53691, 71782, 0, 1, 1, 1, 1),
(53691, 71785, 0, 1, 0, 1, 1),
(53691, 71787, 0, 1, 1, 1, 1);

DELETE FROM `creature_loot_template` WHERE entry = 53691;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES 
(53691, 1, 100, 1, 1, -53691, 3),
(53691, 69237, 100, 1, 0, 1, 2),
(53691, 71141, 100, 1, 0, 1, 3);Update `creature_template` SET AIName="SmartAI" WHERE entry in (53134, 53130, 52447, 53631, 53127, 53096, 54161, 53223, 53639, 53121, 53222, 53119, 54073, 53185, 53120, 53640, 53188, 53224, 53244, 52620, 53128, 53206, 53616, 53622, 52577, 54144, 53095, 53617, 54143, 53115, 53141, 53094, 53167, 53732);
DELETE FROM `smart_scripts` WHERE `entryorguid`=53134 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53134,0,0,0,0,0,100,0,10000,10000,15000,15000,11,99693,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"dinner time-ancient core hound");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53134 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53134,0,1,0,0,0,100,0,5000,5000,10000,10000,11,99736,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"flame breath- ancient core hound");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53134 AND `id`=2 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53134,0,2,0,0,0,100,0,1000,1000,20000,20000,11,99692,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"terrifying roar- ancient core hound");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53130 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53130,0,0,0,0,0,100,0,15000,17000,20000,25000,11,97549,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"lava shower- ancient lava dweller");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53130 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53130,0,1,0,0,0,100,0,3000,3000,9000,9000,11,97306,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"lava spit- ancient lava dweller");

DELETE FROM `smart_scripts` WHERE `entryorguid`=52447;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (52447,0,0,0,0,0,100,0,5000,5000,15000,15000,11,97079,0,0,0,0,0,17,0,6,0,0.0,0.0,0.0,0.0,"seeping venom-cinderweb spiderling");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53631;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53631,0,0,0,0,0,100,0,5000,5000,15000,15000,11,97079,0,0,0,0,0,17,0,6,0,0.0,0.0,0.0,0.0,"seeping venom-cinderweb spiderling");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53127 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53127,0,0,0,6,0,100,0,0,0,0,0,11,99993,0,0,0,0,0,17,0,15,0,0.0,0.0,0.0,0.0,"fiery blood- fire scorpion");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53127 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53127,0,1,0,0,0,100,0,1000,1000,6000,6000,11,99984,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"slightly warm pinchers");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53096 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53096,0,0,0,0,0,100,0,5000,5000,15000,15000,11,100263,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"shell spin-fire turtle hatchling");

DELETE FROM `smart_scripts` WHERE `entryorguid`=54161 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (54161,0,0,0,0,0,100,0,5000,5000,20000,20000,11,100795,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"flame torrent-flame archon");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53223 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53223,0,0,0,0,0,100,0,1000,1000,9000,9000,11,99695,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"flaming spear-flamewaker beast handler");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53639 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53639,0,0,0,2,0,100,0,0,90,8000,8000,11,99618,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"cauterize-flamewaker cauterizer");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53639 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53639,0,1,0,0,0,100,0,5000,5000,15000,15000,11,99625,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"conflagration-flamewaker cauterizer");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53639 AND `id`=2 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53639,0,2,0,0,0,100,0,10000,10000,30000,30000,11,1000060,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"flame shield-flamewaker cauterizer");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53121 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53121,0,0,0,2,0,100,0,0,90,8000,8000,11,99618,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"cauterize-flamewaker cauterizer");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53121 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53121,0,1,0,0,0,100,0,5000,5000,15000,15000,11,99625,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"conflagration-flamewaker cauterizer");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53222 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53222,0,0,0,0,0,100,0,5000,5000,7000,7000,11,16856,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"mortal strike-flamewaker centurion");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53119 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53119,0,0,0,0,0,100,0,5000,5000,8000,8000,11,78660,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"devastate-flamewaker forward guard");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53119 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53119,0,1,0,0,0,100,0,10000,10000,10000,10000,11,99610,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"shockwave-flamewaker forward guard");

DELETE FROM `smart_scripts` WHERE `entryorguid`=54073 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (54073,0,0,0,0,0,100,0,10000,10000,20000,20000,11,100778,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"release the hounds!- flamewaker hound master");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53120 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53120,0,0,0,0,0,100,0,1000,1000,6000,6000,11,99800,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"ensnare-flamewaker pathfinder");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53120 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53120,0,1,0,0,0,100,0,6000,8000,6000,8000,11,99695,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"flaming spear-flamewaker pathfinder");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53640 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53640,0,0,0,0,0,100,0,1000,5000,5000,8000,11,99695,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"flaming spear-flamewaker sentinel");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53188 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53188,0,0,0,0,0,100,0,1000,1000,8000,8000,11,100526,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"blistering wound-flamewaker subjugator");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53224 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53224,0,0,0,0,0,100,11,0,0,1000,1000,11,100743,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"aura of indomitability-flamewaker taskmaster");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53224 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53224,0,1,0,0,0,100,21,0,0,1000,1000,11,100779,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"aura of indomitability-flamewaker taskmaster");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53224 AND `id`=2 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53224,0,2,0,0,0,100,0,1000,5000,5000,10000,11,16856,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"mortal strike-flamewaker taskmaster");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53244 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53244,0,0,0,0,0,100,0,1000,3000,4000,8000,11,78660,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"devastate-flamewaker trainee");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53244 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53244,0,1,0,0,0,100,0,5000,8000,15000,15000,11,99800,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"ensnare-flamewaker trainee");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53244 AND `id`=2 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53244,0,2,0,0,0,100,0,6000,8000,10000,10000,11,99695,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"flaming spear-flamewaker trainee");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53244 AND `id`=3 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53244,0,3,0,0,0,100,0,7000,7000,7000,7000,11,16856,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"mortal strike-flamewaker trainee");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53128 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53128,0,0,0,0,0,100,0,1000,2000,5000,7000,11,99812,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"flame pinchers-giant fire scorpions");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53206 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53206,0,0,0,0,0,100,0,1000,1000,2000,2000,11,100057,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"rend flesh-hell hound");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53616 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53616,0,0,0,0,0,100,0,5000,5000,16000,16000,11,99567,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"soul burn-kar the everburning");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53616 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53616,0,1,0,0,0,100,0,8000,8000,8000,10000,11,99601,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"summon fire elemental-kar the everburning");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53616 AND `id`=2 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53616,0,2,0,0,0,100,0,13000,13000,7000,18000,11,99575,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"summon lava spawn");

UPDATE `creature_template` Set `unit_class`=2 where entry=53622;
UPDATE `creature_template` Set `mana_mod`=800 where entry=53622;
DELETE FROM `smart_scripts` WHERE `entryorguid`=53622 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53622,0,0,0,0,0,100,0,100,100,2000,2000,11,99576,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"fireball-lava spawn");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53622 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53622,0,1,0,0,0,100,0,20000,20000,20000,20000,11,101050,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"split-lava spawn");

DELETE FROM `smart_scripts` WHERE `entryorguid`=54144 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (54144,0,0,0,6,0,100,0,0,0,0,0,11,100755,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"eruption-magmakin");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53095 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53095,0,0,0,0,0,100,0,5000,5000,15000,15000,11,100263,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"shell spin-matriarch fire turtle");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53617 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53617,0,0,0,0,0,100,0,6000,6000,6000,6000,11,99579,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"molten bolt-molten erupter");

DELETE FROM `smart_scripts` WHERE `entryorguid`=54143 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (54143,0,0,0,0,0,100,0,8000,8000,20000,20000,11,100724,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"earthquake-molten flamefather");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53115 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53115,0,0,0,0,0,100,0,5000,5000,5000,5000,11,99530,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"flame stomp-molten lord");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53115 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53115,0,1,0,0,0,100,20,100,100,6000,6000,11,100767,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"melt armor-molten lord");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53115 AND `id`=2 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53115,0,2,0,0,0,100,10,100,100,6000,6000,11,99532,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"melt armor-molten lord");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53115 AND `id`=3 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53115,0,3,4,0,0,100,0,4000,4000,7000,7000,11,99555,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"summon lava jets-molten lord");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53115 AND `id`=4 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53115,0,4,0,0,0,100,0,4000,4000,7000,7000,11,99538,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"summon lava jet-molten lord");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53141 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53141,0,0,0,0,0,100,0,1000,1000,10000,20000,11,100012,0,0,0,0,0,17,10,70,0,0.0,0.0,0.0,0.0,"surge-molten surger");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53094 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53094,0,0,0,0,0,100,0,5000,5000,10000,20000,11,100418,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"flame breath-patriarch fire turtle");
DELETE FROM `smart_scripts` WHERE `entryorguid`=53094 AND `id`=1 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53094,0,1,0,0,0,100,0,15000,20000,15000,20000,11,100842,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"shell shield");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53167 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53167,0,0,0,0,0,100,0,5000,5000,8000,16000,11,101166,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"ignite-unbound pyrelord");

DELETE FROM `smart_scripts` WHERE `entryorguid`=53732 AND `id`=0 AND `source_type`=0 LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (53732,0,0,0,0,0,100,0,0,0,10000,10000,75,99918,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"blazing flame-unbound smoldering elemental");
DELETE FROM `spell_script_names` WHERE `spell_id` IN(99353, -99353);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (99353, 'spell_decimating_strike');
