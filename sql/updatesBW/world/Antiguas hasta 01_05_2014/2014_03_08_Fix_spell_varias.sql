-- PALADIN BUSQUEDA DE JUSTICIA: AHORA DEBERIA DAR UNA CARGA AL SER ESTUNEADO

DELETE FROM spell_proc_event WHERE entry in(26022, 26023);
INSERT INTO `spell_proc_event` VALUES (26022, 0, 0, 0, 0, 0, 1073741824, 65536, 0, 50, 0);
INSERT INTO `spell_proc_event` VALUES (26023, 0, 0, 0, 0, 0, 1073741824, 65536, 0, 100, 0);


-- PICARO GLIFO DE PUÑALADA 

DELETE FROM spell_proc_event WHERE entry = 56800;
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES 
(56800, 0, 8, 8388612, 0, 0, 16, 2, 0, 0, 0);

-- FUEGO SOLAR DRUIDA ELEMENTAL 

DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=48517;
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES (48517,94338,2,'Eclipse (solar) - Sunfire');


