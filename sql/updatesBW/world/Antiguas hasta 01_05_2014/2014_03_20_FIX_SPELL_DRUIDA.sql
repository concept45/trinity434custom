-- FIX SPELL DRUIDA

-- ESTRELLAS FUGASES DRUIDA EQUILIBRIO
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=78674 AND `spell_effect`=93400;
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=78674 AND `spell_effect`=-93400;
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES 
(78674,-93400,0,'Starsurge removes Shooting Stars');


-- LLUVIA LUNAR DRUIDA EQUILIBRIO

-- spell script name
DELETE FROM `spell_script_names` WHERE `spell_id` IN (8921,93402);
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES 
(8921,'spell_dru_lunar_shower'),
(93402,'spell_dru_lunar_shower');

-- spell proc 
DELETE FROM `spell_proc_event` WHERE `entry` in (33603,33604,33605);
INSERT INTO `spell_proc_event` (`entry`,`SpellFamilyMask0`) VALUES 
('33603','2'),
('33604','2'),
('33605','2');

-- PRESTEZA FERAL REPARACION DE TALENTO SUS "BENEFICIOS"

-- spell linked spell
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=24867 AND `spell_effect`=17002;
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=24864 AND `spell_effect`=24866;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(24867, 17002, 2, 'Feral Swiftness (Rank 1)'),
(24864, 24866, 2, 'Feral Swiftness (Rank 2)');

-- spell script name
DELETE FROM `spell_script_names` WHERE `spell_id` IN (1850,77761,77764);
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES 
(1850,'spell_dru_feral_swiftness'),
(77761,'spell_dru_feral_swiftness'),
(77764,'spell_dru_feral_swiftness');

-- PULVERIZAR DRUIDA FERAL
DELETE FROM `spell_script_names` WHERE `spell_id`=80313;
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES 
(80313,'spell_dru_pulverize');

-- RABIA FERAL

-- spell script name
DELETE FROM `spell_script_names` WHERE `spell_id`=50334;
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES 
(50334,'spell_dru_berserk');

-- glifo de rabia
DELETE FROM `spell_proc_event` WHERE `entry`=58096;
INSERT INTO `spell_proc_event` (`entry`, `SpellFamilyMask0`, `SpellFamilyMask1`) VALUES 
(58096, 268435456, 67108864);

-- LACERAR FERAL
DELETE FROM `spell_script_names` WHERE `spell_id`=33745;
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES 
(33745,'spell_dru_lacerate');