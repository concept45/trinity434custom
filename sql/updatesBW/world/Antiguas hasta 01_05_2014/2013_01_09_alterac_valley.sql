UPDATE creature_template SET EXP = 3, Health_mod = 35, mindmg = 55000, maxdmg = 60000, dmg_multiplier = 1 WHERE entry = 13798;
UPDATE creature_template SET EXP = 3, mindmg = 15000, maxdmg = 25000, dmg_multiplier = 1, minlevel = 84, maxlevel = 84, Health_mod = 20 WHERE entry IN (13326, 13328);
UPDATE creature_template SET EXP = 2, Health_mod = 2 WHERE entry = 10981;
UPDATE creature_template SET EXP = 3, Health_mod = 2.5, mindmg = 35000, maxdmg = 45000, dmg_multiplier = 1 WHERE entry = 10982;
UPDATE creature_template SET EXP = 2, Health_mod = 2 WHERE entry = 10990;
UPDATE creature_template SET EXP = 3, mindmg = 25000, maxdmg = 35000, dmg_multiplier = 1 WHERE entry = 12050;
UPDATE creature_template SET EXP = 3 WHERE entry = 14284;
UPDATE creature_template SET EXP = 3, minlevel = 85, maxlevel = 85, mindmg = 25000, maxdmg = 35000, dmg_multiplier = 1, Health_mod = 10 WHERE entry = 25040; -- Elemental
UPDATE creature_template SET Health_mod = 250, mindmg = 80000, maxdmg = 90000, dmg_multiplier = 1 WHERE entry = 11949; -- 1r boss alianza
UPDATE creature_template SET Health_mod = 250, mindmg = 95000, maxdmg = 115000, dmg_multiplier = 1 WHERE entry = 10981; -- 1r boss horda
UPDATE creature_template SET Health_mod = 127.5, mindmg = 65000, maxdmg = 75000, dmg_multiplier = 1 WHERE entry = 11946; -- Drek'thar
UPDATE creature_template SET Health_mod = 80, mindmg = 55000, maxdmg = 65000, dmg_multiplier = 1 WHERE entry IN (14772, 14773, 14777, 14776);  -- Maestros de guerra horda
UPDATE creature_template SET EXP = 3, Health_mod = 10, mindmg = 95000, maxdmg = 125000, dmg_multiplier = 1 WHERE entry = 10982;
UPDATE creature_template SET Health_mod = 127.5, mindmg = 65000, maxdmg = 75000, dmg_multiplier = 1 WHERE entry = 11948; -- Vandaar
UPDATE creature_template SET Health_mod = 80, mindmg = 55000, maxdmg = 65000, dmg_multiplier = 1 WHERE entry IN (14764, 14763, 14762, 14765);  -- Maestros de guerra alianza
UPDATE creature_template SET EXP = 3 WHERE entry = 13816;

DELETE FROM disables WHERE sourceType = 3 AND entry = 1;