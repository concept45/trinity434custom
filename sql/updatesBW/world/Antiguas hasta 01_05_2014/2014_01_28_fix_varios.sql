-- logros
INSERT INTO `achievement_reward` (entry, title_A, title_H, item, sender, SUBJECT, TEXT) VALUES ( 5827, 267, 267, 0, 0, '', '');
INSERT INTO `achievement_reward` (entry, title_A, title_H, item, sender, SUBJECT, TEXT) VALUES ( 5116, 229, 229, 0, 0, '', '');

-- teleport ulduar
UPDATE `gameobject_template` SET `flags`=32 WHERE `entry`=194569;

-- Bloodlord Mandokir - Fix 
SET @ENTRY := 52151;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,@SOURCETYPE,0,0,5,0,100,0,0,0,1,0,11,96662,1,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Mandokir - Subir de nivel - Zulgurub"),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,30000,32000,30000,32000,11,96743,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Mandokir - Embate - Zulgurub"),
(@ENTRY,@SOURCETYPE,2,0,0,0,100,0,40000,41000,40000,41000,11,96776,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Mandokir - Bloodletting - Zulgurub"),
(@ENTRY,@SOURCETYPE,3,0,0,0,100,0,120000,130000,120000,130000,11,96684,1,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Mandokir - Decapitar - Zulgurub"),
(@ENTRY,@SOURCETYPE,4,0,0,0,100,0,140000,145000,140000,145000,11,96684,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Mandokir - Frenesi - Zulgurub"),
(@ENTRY,@SOURCETYPE,5,0,5,0,100,0,0,0,1,0,1,2,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Mandokir - Subir de nivel say- Zulgurub");

-- fix exploit para que solo los DK puedan utilizar el grifo de la plaga para evitar que otros aprendan todas las rutas de vuelo
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=18 AND `SourceGroup`=29501 AND `SourceEntry`=54575;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(18, 29501, 54575, 0, 0, 15, 0, 32, 0, 0, 0, 0, 0, '', 'Spellclick only for dks');


