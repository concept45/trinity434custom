
// FIXED [La Batalla debe Continuar]  [URL MISION http://es.wowhead.com/quest=11537/la-batalla-debe-continuar]

UPDATE `worldtest`.`quest_template` SET `RequiredNpcOrGo1`='25001',`RequiredNpcOrGo2`='25002',`RequiredNpcOrGo3`='24999',`RequiredNpcOrGo4`='25068',`RequiredNpcOrGoCount1`='6',`RequiredNpcOrGoCount3`='6',`RequiredNpcOrGoCount4`='6' WHERE `Id`='11537';



// FIXED [Conoce tus Lineas Ley]  [URL MISION http://es.wowhead.com/quest=11547]


UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry` IN (25156,25154,25157);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (25156,25154,25157) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(25156,0,0,0,8,0,100,0,45191,0,0,0,33,25156,0,0,0,0,0,7,0,0,0,0,0,0,0,'Sunwell - Quest Bunny - Portal - On Spell Hit(Sample Ley Line Field) - Give Quest Credit'),
(25154,0,0,0,8,0,100,0,45191,0,0,0,33,25154,0,0,0,0,0,7,0,0,0,0,0,0,0,'Sunwell - Quest Bunny - Shrine - On Spell Hit(Sample Ley Line Field) - Give Quest Credit'),
(25157,0,0,0,8,0,100,0,45191,0,0,0,33,25157,0,0,0,0,0,7,0,0,0,0,0,0,0,'Sunwell - Quest Bunny - Sunwell - On Spell Hit(Sample Ley Line Field) - Give Quest Credit');