-- Teleport Darnassus for Mage
DELETE FROM `spell_target_position` WHERE (`id`='3565');
INSERT INTO `spell_target_position` (`id`, `target_map`, `target_position_x`, `target_position_y`, `target_position_z`, `target_orientation`) VALUES (3565, 1, 9660.81, 2513.64, 1331.66, 3.06);

-- Teleport Theramore for Mage
DELETE FROM `spell_target_position` WHERE (`id`='49359');
INSERT INTO `spell_target_position` (`id`, `target_map`, `target_position_x`, `target_position_y`, `target_position_z`, `target_orientation`) VALUES (49359, 1, -3730.72, -4422.21, 30.4836, 0.810732);

-- Teleport Tolbarad for Mage Ally
DELETE FROM `spell_target_position` WHERE (`id`='88342');
INSERT INTO `spell_target_position` (`id`, `target_map`, `target_position_x`, `target_position_y`, `target_position_z`, `target_orientation`) VALUES (88342, 732, -344.6, 1043.8, 21.5, 1.5);

-- Teleport Tolbarad for Mage Horde
DELETE FROM `spell_target_position` WHERE (`id`='88344');
INSERT INTO `spell_target_position` (`id`, `target_map`, `target_position_x`, `target_position_y`, `target_position_z`, `target_orientation`) VALUES (88344, 732, -601.4, 1382.03, 21.9, 1.5);
